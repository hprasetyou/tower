<?php

class Manage_towers extends MY_Controller{


  function __construct(){
	parent::__construct();
	$this->set_objname('Tower');
	$this->tpl = 'admin/towers';
  }

  function delete($id){
		$data = parent::delete($id);
		redirect('manage_towers');
  }

  	function write($id=null){
      if($this->input->post('PartnerId')>0){
        $partner = 'PartnerId';
      }else{
        $newpartner = new TowerOwner;
        $newpartner->setName($this->input->post('PartnerId'));
        $newpartner->save();
        $partner = array(
          'value'=> $newpartner->getId()
        );
      }
      $this->form['PartnerId'] = $partner;
      if(!$id){

        $this->load->helper('good_numbering');
        $this->form['Name'] = array('value'=>create_number(array(
          'format'=>'T-i',
          'tb_name'=>'tower',
          'tb_field'=>'name'
        )));
      }
  		$data = parent::write($id);
      if($this->input->is_ajax_request()){
  			echo $data->toJSON();
  		}else{
  			redirect('back/admin/manage_towers/detail/'.$data->getId());
  		}
  	}
}
