<?php

class Manage_configurations extends MY_Controller{


  function __construct(){
    parent::__construct();
		$this->set_objname('Configuration');
		$this->tpl = 'admin/configurations';

    $this->authorization->check_authorization('manage_configurations');
  }


	function write($id=null){
		$data = parent::write($id);
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('back/admin/manage_configurations/detail/'.$data->getId());
		}
	}

  function configure_email(){
    $conf = $this->config->item('email');
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $cpath = __DIR__.'/../../../config/config.php';
      $str=file_get_contents($cpath);
      $newdata = $this->input->post('conf');
      foreach ($conf as $key => $value) {
        # code...
        $newval = $newdata[$key];
        $str=str_replace("\$config['email']['$key'] = '$value';", "\$config['email']['$key'] = '$newval';",$str);
      }
      file_put_contents($cpath, $str);
      $this->load->library('mailer');
      $this->mailer
      ->set_recipient($this->input->post('testEmail'))
      ->set_recipient_name($this->input->post('testEmail'))
      ->set_subject('test')
      ->set_body('test')->send_email();
      $this->session->set_flashdata('info','Konfigurasi berhasil');
      redirect('/back/admin/manage_configurations/configure_email');

    }else{
      $this->template->render('admin/admin/configurations/email',array(
        'configuration'=>$conf
      ));
    }

  }

  function delete($id){
		$data = parent::delete($id);
		redirect('manage_configurations');
  }

}
