<?php

class Manage_towerrecsubmissions extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->objname = 'TowerRecSubmission';
        $this->tpl = 'admin/towerrecsubmissions';
        // $this->authorization->check_authorization('manage_towerrecsubmissions');
    }
    public function delete($id)
    {
        $data = parent::delete($id);
        redirect('manage_towerrecsubmissions');
    }
    public function get_json()
    {
        $this->custom_column['company'] = "_{Partner}_->getCompany()?_{Partner}_->getCompany()->getName():''";
        parent::get_json();
    }

    public function set_status($status, $id, $with_email = null)
    {
        $towerrec = TowerRecSubmissionQuery::create()->findPk($id);
        $content = $towerrec->get_letter();
        $conf = ConfigurationQuery::create();

        $msg_body = $conf
        ->findOneByName("email_recommendation_letter_$status")
        ->getDataValue();
        $req = TowerRecSubmissionQuery::create()->findPk($id);
        $variables = array(
          '_ReviewerNote_' => $this->input->post('Reason')
        );
        foreach ($variables as $key => $value) {
          $msg_body = str_replace($key,$value,$msg_body);
        }
        $msg = array(
            'recipient'=>$req->getPartner()->getEmail(),
            'subject'=>'Pemberitahuan',
            'recipient_name'=>$req->getPartner()->getName(),
            'mail_tmpl' => 'mail/layout',
            'mail_tmpl_data'=>array(
              'recipient_name' => $req->getPartner()->getName(),
              'message_body'=>$msg_body
            ),
        );
        if($status == 'accepted'){
          $pdf = $this->template->render_pdf(
          'admin/applicant/towerrecsubmissions/pdf/template',
              array('content'=>$content),
              array(
                'docname'=>'Surat Rekomendasi Pendirian Menara '.$towerrec->getName(),
                'render'=>false
              ));
              $msg['attachments'] = array(
                array(
                    'name' => $pdf['name'],
                    'path' => $pdf['path']
                ));
        }

        $req->setReviewerNote($this->input->post('Reason'));
        $req->save();
        parent::set_status($status, $id, $msg);
    }
}
