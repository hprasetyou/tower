import 'jquery'
// import 'typeahead'
var AutoComplete = require("autocomplete-js");
import kudus from '../../data/kudus.json'

var marker
import {setBound, placeMarker} from '../map_func'
$('#btn-edit').click(function(){
	$('#map-tower-rec-submission').data('editable','1');
})
$('#btn-canceledit').click(function(){
	$('#map-tower-rec-submission').data('editable',false);
})

function geocodePlaceId(geocoder, map, latlng) {
  geocoder.geocode({
    'location': latlng
  }, function(results, status) {
    if (status === 'OK') {
      if (results[0]) {
        var loc = results[0];
        $('#Location,#District,#Village').val('')
        $('#Location').val(loc.formatted_address)
        $('#District').val(loc.address_components[2].long_name)
        $('#Village').val(loc.address_components[1].long_name)
        $('input').each(function(){
          if(!(this.value == '' || this.value == this.defaultValue)){
            $(this).attr('readonly','readonly')
          }
        })
      } else {
        window.alert('No results found');
      }
    } else {
      window.alert('Geocoder failed due to: ' + status);
    }
  });
}


function initMap() {
  $.ajax({
    'url': '/index.php/get_all_cellplan?with_tower=1',
    'dataType': 'JSON'
  }).done(function(data) {
		console.log(data);
		var o = data.cellplan
		var rad = data.radius

		var towers = data.tower
    var geocoder = new google.maps.Geocoder;
    var map = new google.maps.Map(document.getElementById('map-tower-rec-submission'), {
      zoom: 12,
      center: {
        lat: -6.801039,
        lng: 110.843246
      },
      streetViewControl: false,
      fullscreenControl: false,
      showRoadLabels: true,
      mapTypeId: 'terrain'
    });

    setBound(map)
		var pre_latlng = {}
    if($('#map-tower-rec-submission').data('marker-lat')){
			pre_latlng = {'lat':$('#map-tower-rec-submission').data('marker-lat'),'lng':$('#map-tower-rec-submission').data('marker-lng')}
      placeMarker(map,pre_latlng)
    }
    // Construct the circle for each value in citymap.
    // Note: We scale the area of the circle based on the population.
    for (var plan in o) {
      // Add the circle for this city to the map.
      var cellPlan = new google.maps.Circle({
        strokeColor: '#00AA00',
        strokeOpacity: 0.5,
        strokeWeight: 2,
        fillColor: '#00AA00',
        fillOpacity: 0.35,
        map: map,
        center: o[plan].LatLng,
        radius: rad
      });

      google.maps.event.addListener(cellPlan, "click", function(event) {
        console.log(this);
        var cellC = this.getCenter().toJSON()
        var cellCenter = new google.maps.LatLng(cellC.lat, cellC.lng)
        var latitude = event.latLng.lat();
        var longitude = event.latLng.lng();
        var latlng = new google.maps.LatLng(latitude, longitude)
          console.log(latlng);
        if($('#map-tower-rec-submission').data('editable')){
          placeMarker(map, latlng)
          $('#DistanceFromCenter').val(google.maps.geometry.spherical.computeDistanceBetween(latlng, cellCenter));
          geocodePlaceId(geocoder, map, latlng);
          $('#Latitude').val(latitude)
          $('#Longitude').val(longitude)
        }

        if (map.getZoom() < 15) {

          map.setZoom(16);
          map.setCenter(latlng);
        }
      });
    }
		for (var i in towers){
			var tower = new google.maps.Marker({
			map:map,
			position: towers[i].LatLng,
			icon: '/public/assets/img/communication-tower-icon.png'
		});
		}
  })

}
AutoComplete();


window.initMap = initMap;
