<?php

class Manage_partners extends MY_Controller{


  function __construct(){
    $this->pass_method = ['get_json','get_companies'];
    parent::__construct();
		$this->set_objname('Partner');
		$this->tpl = 'admin/partners';
  }


	function write($id=null){
		$data = parent::write($id);
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('manage_partners/detail/'.$data->getId());
		}
	}

  function get_employees(){

  }

  function get_companies(){
    $o = array('results'=>[]);
    $o['results'] = [];
    if($this->input->get('search')){
      $companies = TowerApplicantQuery::create()
      ->where('Partner.CompanyId IS NULL')
      ->where('Partner.Name LIKE ?',"%{$this->input->get('search')}%")
      ->find();
      foreach ($companies as $key => $value) {
        # code...
        $o['results'][$key]['id'] = $value->getId();
        $o['results'][$key]['text'] = $value->getName();
      }      
    }

    header('Content-Type: application/json');
    echo json_encode($o);
  }

  function delete($id){
		$data = parent::delete($id);
		redirect('manage_partners');
  }

}
