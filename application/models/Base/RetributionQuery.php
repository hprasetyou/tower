<?php

namespace Base;

use \Retribution as ChildRetribution;
use \RetributionQuery as ChildRetributionQuery;
use \Exception;
use \PDO;
use Map\RetributionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'retribution' table.
 *
 *
 *
 * @method     ChildRetributionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildRetributionQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildRetributionQuery orderByPaymentDate($order = Criteria::ASC) Order by the payment_date column
 * @method     ChildRetributionQuery orderByPaymentAmount($order = Criteria::ASC) Order by the payment_amount column
 * @method     ChildRetributionQuery orderByTowerId($order = Criteria::ASC) Order by the tower_id column
 * @method     ChildRetributionQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildRetributionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildRetributionQuery groupById() Group by the id column
 * @method     ChildRetributionQuery groupByName() Group by the name column
 * @method     ChildRetributionQuery groupByPaymentDate() Group by the payment_date column
 * @method     ChildRetributionQuery groupByPaymentAmount() Group by the payment_amount column
 * @method     ChildRetributionQuery groupByTowerId() Group by the tower_id column
 * @method     ChildRetributionQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildRetributionQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildRetributionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRetributionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRetributionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRetributionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRetributionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRetributionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRetributionQuery leftJoinTower($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tower relation
 * @method     ChildRetributionQuery rightJoinTower($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tower relation
 * @method     ChildRetributionQuery innerJoinTower($relationAlias = null) Adds a INNER JOIN clause to the query using the Tower relation
 *
 * @method     ChildRetributionQuery joinWithTower($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tower relation
 *
 * @method     ChildRetributionQuery leftJoinWithTower() Adds a LEFT JOIN clause and with to the query using the Tower relation
 * @method     ChildRetributionQuery rightJoinWithTower() Adds a RIGHT JOIN clause and with to the query using the Tower relation
 * @method     ChildRetributionQuery innerJoinWithTower() Adds a INNER JOIN clause and with to the query using the Tower relation
 *
 * @method     \TowerQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRetribution findOne(ConnectionInterface $con = null) Return the first ChildRetribution matching the query
 * @method     ChildRetribution findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRetribution matching the query, or a new ChildRetribution object populated from the query conditions when no match is found
 *
 * @method     ChildRetribution findOneById(int $id) Return the first ChildRetribution filtered by the id column
 * @method     ChildRetribution findOneByName(string $name) Return the first ChildRetribution filtered by the name column
 * @method     ChildRetribution findOneByPaymentDate(string $payment_date) Return the first ChildRetribution filtered by the payment_date column
 * @method     ChildRetribution findOneByPaymentAmount(int $payment_amount) Return the first ChildRetribution filtered by the payment_amount column
 * @method     ChildRetribution findOneByTowerId(int $tower_id) Return the first ChildRetribution filtered by the tower_id column
 * @method     ChildRetribution findOneByCreatedAt(string $created_at) Return the first ChildRetribution filtered by the created_at column
 * @method     ChildRetribution findOneByUpdatedAt(string $updated_at) Return the first ChildRetribution filtered by the updated_at column *

 * @method     ChildRetribution requirePk($key, ConnectionInterface $con = null) Return the ChildRetribution by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRetribution requireOne(ConnectionInterface $con = null) Return the first ChildRetribution matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRetribution requireOneById(int $id) Return the first ChildRetribution filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRetribution requireOneByName(string $name) Return the first ChildRetribution filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRetribution requireOneByPaymentDate(string $payment_date) Return the first ChildRetribution filtered by the payment_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRetribution requireOneByPaymentAmount(int $payment_amount) Return the first ChildRetribution filtered by the payment_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRetribution requireOneByTowerId(int $tower_id) Return the first ChildRetribution filtered by the tower_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRetribution requireOneByCreatedAt(string $created_at) Return the first ChildRetribution filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRetribution requireOneByUpdatedAt(string $updated_at) Return the first ChildRetribution filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRetribution[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRetribution objects based on current ModelCriteria
 * @method     ChildRetribution[]|ObjectCollection findById(int $id) Return ChildRetribution objects filtered by the id column
 * @method     ChildRetribution[]|ObjectCollection findByName(string $name) Return ChildRetribution objects filtered by the name column
 * @method     ChildRetribution[]|ObjectCollection findByPaymentDate(string $payment_date) Return ChildRetribution objects filtered by the payment_date column
 * @method     ChildRetribution[]|ObjectCollection findByPaymentAmount(int $payment_amount) Return ChildRetribution objects filtered by the payment_amount column
 * @method     ChildRetribution[]|ObjectCollection findByTowerId(int $tower_id) Return ChildRetribution objects filtered by the tower_id column
 * @method     ChildRetribution[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildRetribution objects filtered by the created_at column
 * @method     ChildRetribution[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildRetribution objects filtered by the updated_at column
 * @method     ChildRetribution[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RetributionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RetributionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Retribution', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRetributionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRetributionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRetributionQuery) {
            return $criteria;
        }
        $query = new ChildRetributionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRetribution|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RetributionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RetributionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRetribution A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, payment_date, payment_amount, tower_id, created_at, updated_at FROM retribution WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRetribution $obj */
            $obj = new ChildRetribution();
            $obj->hydrate($row);
            RetributionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRetribution|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RetributionTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RetributionTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RetributionTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RetributionTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RetributionTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RetributionTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the payment_date column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentDate('2011-03-14'); // WHERE payment_date = '2011-03-14'
     * $query->filterByPaymentDate('now'); // WHERE payment_date = '2011-03-14'
     * $query->filterByPaymentDate(array('max' => 'yesterday')); // WHERE payment_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $paymentDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function filterByPaymentDate($paymentDate = null, $comparison = null)
    {
        if (is_array($paymentDate)) {
            $useMinMax = false;
            if (isset($paymentDate['min'])) {
                $this->addUsingAlias(RetributionTableMap::COL_PAYMENT_DATE, $paymentDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paymentDate['max'])) {
                $this->addUsingAlias(RetributionTableMap::COL_PAYMENT_DATE, $paymentDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RetributionTableMap::COL_PAYMENT_DATE, $paymentDate, $comparison);
    }

    /**
     * Filter the query on the payment_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentAmount(1234); // WHERE payment_amount = 1234
     * $query->filterByPaymentAmount(array(12, 34)); // WHERE payment_amount IN (12, 34)
     * $query->filterByPaymentAmount(array('min' => 12)); // WHERE payment_amount > 12
     * </code>
     *
     * @param     mixed $paymentAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function filterByPaymentAmount($paymentAmount = null, $comparison = null)
    {
        if (is_array($paymentAmount)) {
            $useMinMax = false;
            if (isset($paymentAmount['min'])) {
                $this->addUsingAlias(RetributionTableMap::COL_PAYMENT_AMOUNT, $paymentAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paymentAmount['max'])) {
                $this->addUsingAlias(RetributionTableMap::COL_PAYMENT_AMOUNT, $paymentAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RetributionTableMap::COL_PAYMENT_AMOUNT, $paymentAmount, $comparison);
    }

    /**
     * Filter the query on the tower_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTowerId(1234); // WHERE tower_id = 1234
     * $query->filterByTowerId(array(12, 34)); // WHERE tower_id IN (12, 34)
     * $query->filterByTowerId(array('min' => 12)); // WHERE tower_id > 12
     * </code>
     *
     * @see       filterByTower()
     *
     * @param     mixed $towerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function filterByTowerId($towerId = null, $comparison = null)
    {
        if (is_array($towerId)) {
            $useMinMax = false;
            if (isset($towerId['min'])) {
                $this->addUsingAlias(RetributionTableMap::COL_TOWER_ID, $towerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($towerId['max'])) {
                $this->addUsingAlias(RetributionTableMap::COL_TOWER_ID, $towerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RetributionTableMap::COL_TOWER_ID, $towerId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(RetributionTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(RetributionTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RetributionTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(RetributionTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(RetributionTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RetributionTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Tower object
     *
     * @param \Tower|ObjectCollection $tower The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRetributionQuery The current query, for fluid interface
     */
    public function filterByTower($tower, $comparison = null)
    {
        if ($tower instanceof \Tower) {
            return $this
                ->addUsingAlias(RetributionTableMap::COL_TOWER_ID, $tower->getId(), $comparison);
        } elseif ($tower instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RetributionTableMap::COL_TOWER_ID, $tower->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTower() only accepts arguments of type \Tower or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tower relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function joinTower($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tower');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tower');
        }

        return $this;
    }

    /**
     * Use the Tower relation Tower object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TowerQuery A secondary query class using the current class as primary query
     */
    public function useTowerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTower($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tower', '\TowerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRetribution $retribution Object to remove from the list of results
     *
     * @return $this|ChildRetributionQuery The current query, for fluid interface
     */
    public function prune($retribution = null)
    {
        if ($retribution) {
            $this->addUsingAlias(RetributionTableMap::COL_ID, $retribution->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the retribution table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RetributionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RetributionTableMap::clearInstancePool();
            RetributionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RetributionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RetributionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RetributionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RetributionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RetributionQuery
