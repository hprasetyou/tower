<?php

namespace Base;

use \TowerRecSubmission as ChildTowerRecSubmission;
use \TowerRecSubmissionQuery as ChildTowerRecSubmissionQuery;
use \Exception;
use \PDO;
use Map\TowerRecSubmissionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tower_rec_submission' table.
 *
 *
 *
 * @method     ChildTowerRecSubmissionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildTowerRecSubmissionQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildTowerRecSubmissionQuery orderByPartnerId($order = Criteria::ASC) Order by the partner_id column
 * @method     ChildTowerRecSubmissionQuery orderByLongitude($order = Criteria::ASC) Order by the longitude column
 * @method     ChildTowerRecSubmissionQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method     ChildTowerRecSubmissionQuery orderByCellPlanId($order = Criteria::ASC) Order by the cell_plan_id column
 * @method     ChildTowerRecSubmissionQuery orderByDistanceFromCenter($order = Criteria::ASC) Order by the distance_from_center column
 * @method     ChildTowerRecSubmissionQuery orderByHeight($order = Criteria::ASC) Order by the height column
 * @method     ChildTowerRecSubmissionQuery orderByDistrict($order = Criteria::ASC) Order by the district column
 * @method     ChildTowerRecSubmissionQuery orderByVillage($order = Criteria::ASC) Order by the village column
 * @method     ChildTowerRecSubmissionQuery orderByTowerType($order = Criteria::ASC) Order by the tower_type column
 * @method     ChildTowerRecSubmissionQuery orderByLocation($order = Criteria::ASC) Order by the location column
 * @method     ChildTowerRecSubmissionQuery orderByReviewerNote($order = Criteria::ASC) Order by the reviewer_note column
 * @method     ChildTowerRecSubmissionQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildTowerRecSubmissionQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildTowerRecSubmissionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildTowerRecSubmissionQuery groupById() Group by the id column
 * @method     ChildTowerRecSubmissionQuery groupByName() Group by the name column
 * @method     ChildTowerRecSubmissionQuery groupByPartnerId() Group by the partner_id column
 * @method     ChildTowerRecSubmissionQuery groupByLongitude() Group by the longitude column
 * @method     ChildTowerRecSubmissionQuery groupByLatitude() Group by the latitude column
 * @method     ChildTowerRecSubmissionQuery groupByCellPlanId() Group by the cell_plan_id column
 * @method     ChildTowerRecSubmissionQuery groupByDistanceFromCenter() Group by the distance_from_center column
 * @method     ChildTowerRecSubmissionQuery groupByHeight() Group by the height column
 * @method     ChildTowerRecSubmissionQuery groupByDistrict() Group by the district column
 * @method     ChildTowerRecSubmissionQuery groupByVillage() Group by the village column
 * @method     ChildTowerRecSubmissionQuery groupByTowerType() Group by the tower_type column
 * @method     ChildTowerRecSubmissionQuery groupByLocation() Group by the location column
 * @method     ChildTowerRecSubmissionQuery groupByReviewerNote() Group by the reviewer_note column
 * @method     ChildTowerRecSubmissionQuery groupByStatus() Group by the status column
 * @method     ChildTowerRecSubmissionQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildTowerRecSubmissionQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildTowerRecSubmissionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTowerRecSubmissionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTowerRecSubmissionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTowerRecSubmissionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTowerRecSubmissionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTowerRecSubmissionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTowerRecSubmissionQuery leftJoinPartner($relationAlias = null) Adds a LEFT JOIN clause to the query using the Partner relation
 * @method     ChildTowerRecSubmissionQuery rightJoinPartner($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Partner relation
 * @method     ChildTowerRecSubmissionQuery innerJoinPartner($relationAlias = null) Adds a INNER JOIN clause to the query using the Partner relation
 *
 * @method     ChildTowerRecSubmissionQuery joinWithPartner($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Partner relation
 *
 * @method     ChildTowerRecSubmissionQuery leftJoinWithPartner() Adds a LEFT JOIN clause and with to the query using the Partner relation
 * @method     ChildTowerRecSubmissionQuery rightJoinWithPartner() Adds a RIGHT JOIN clause and with to the query using the Partner relation
 * @method     ChildTowerRecSubmissionQuery innerJoinWithPartner() Adds a INNER JOIN clause and with to the query using the Partner relation
 *
 * @method     ChildTowerRecSubmissionQuery leftJoinCellPlan($relationAlias = null) Adds a LEFT JOIN clause to the query using the CellPlan relation
 * @method     ChildTowerRecSubmissionQuery rightJoinCellPlan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CellPlan relation
 * @method     ChildTowerRecSubmissionQuery innerJoinCellPlan($relationAlias = null) Adds a INNER JOIN clause to the query using the CellPlan relation
 *
 * @method     ChildTowerRecSubmissionQuery joinWithCellPlan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CellPlan relation
 *
 * @method     ChildTowerRecSubmissionQuery leftJoinWithCellPlan() Adds a LEFT JOIN clause and with to the query using the CellPlan relation
 * @method     ChildTowerRecSubmissionQuery rightJoinWithCellPlan() Adds a RIGHT JOIN clause and with to the query using the CellPlan relation
 * @method     ChildTowerRecSubmissionQuery innerJoinWithCellPlan() Adds a INNER JOIN clause and with to the query using the CellPlan relation
 *
 * @method     \PartnerQuery|\CellPlanQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTowerRecSubmission findOne(ConnectionInterface $con = null) Return the first ChildTowerRecSubmission matching the query
 * @method     ChildTowerRecSubmission findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTowerRecSubmission matching the query, or a new ChildTowerRecSubmission object populated from the query conditions when no match is found
 *
 * @method     ChildTowerRecSubmission findOneById(int $id) Return the first ChildTowerRecSubmission filtered by the id column
 * @method     ChildTowerRecSubmission findOneByName(string $name) Return the first ChildTowerRecSubmission filtered by the name column
 * @method     ChildTowerRecSubmission findOneByPartnerId(int $partner_id) Return the first ChildTowerRecSubmission filtered by the partner_id column
 * @method     ChildTowerRecSubmission findOneByLongitude(string $longitude) Return the first ChildTowerRecSubmission filtered by the longitude column
 * @method     ChildTowerRecSubmission findOneByLatitude(string $latitude) Return the first ChildTowerRecSubmission filtered by the latitude column
 * @method     ChildTowerRecSubmission findOneByCellPlanId(int $cell_plan_id) Return the first ChildTowerRecSubmission filtered by the cell_plan_id column
 * @method     ChildTowerRecSubmission findOneByDistanceFromCenter(double $distance_from_center) Return the first ChildTowerRecSubmission filtered by the distance_from_center column
 * @method     ChildTowerRecSubmission findOneByHeight(int $height) Return the first ChildTowerRecSubmission filtered by the height column
 * @method     ChildTowerRecSubmission findOneByDistrict(string $district) Return the first ChildTowerRecSubmission filtered by the district column
 * @method     ChildTowerRecSubmission findOneByVillage(string $village) Return the first ChildTowerRecSubmission filtered by the village column
 * @method     ChildTowerRecSubmission findOneByTowerType(string $tower_type) Return the first ChildTowerRecSubmission filtered by the tower_type column
 * @method     ChildTowerRecSubmission findOneByLocation(string $location) Return the first ChildTowerRecSubmission filtered by the location column
 * @method     ChildTowerRecSubmission findOneByReviewerNote(string $reviewer_note) Return the first ChildTowerRecSubmission filtered by the reviewer_note column
 * @method     ChildTowerRecSubmission findOneByStatus(string $status) Return the first ChildTowerRecSubmission filtered by the status column
 * @method     ChildTowerRecSubmission findOneByCreatedAt(string $created_at) Return the first ChildTowerRecSubmission filtered by the created_at column
 * @method     ChildTowerRecSubmission findOneByUpdatedAt(string $updated_at) Return the first ChildTowerRecSubmission filtered by the updated_at column *

 * @method     ChildTowerRecSubmission requirePk($key, ConnectionInterface $con = null) Return the ChildTowerRecSubmission by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOne(ConnectionInterface $con = null) Return the first ChildTowerRecSubmission matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTowerRecSubmission requireOneById(int $id) Return the first ChildTowerRecSubmission filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByName(string $name) Return the first ChildTowerRecSubmission filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByPartnerId(int $partner_id) Return the first ChildTowerRecSubmission filtered by the partner_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByLongitude(string $longitude) Return the first ChildTowerRecSubmission filtered by the longitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByLatitude(string $latitude) Return the first ChildTowerRecSubmission filtered by the latitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByCellPlanId(int $cell_plan_id) Return the first ChildTowerRecSubmission filtered by the cell_plan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByDistanceFromCenter(double $distance_from_center) Return the first ChildTowerRecSubmission filtered by the distance_from_center column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByHeight(int $height) Return the first ChildTowerRecSubmission filtered by the height column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByDistrict(string $district) Return the first ChildTowerRecSubmission filtered by the district column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByVillage(string $village) Return the first ChildTowerRecSubmission filtered by the village column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByTowerType(string $tower_type) Return the first ChildTowerRecSubmission filtered by the tower_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByLocation(string $location) Return the first ChildTowerRecSubmission filtered by the location column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByReviewerNote(string $reviewer_note) Return the first ChildTowerRecSubmission filtered by the reviewer_note column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByStatus(string $status) Return the first ChildTowerRecSubmission filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByCreatedAt(string $created_at) Return the first ChildTowerRecSubmission filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTowerRecSubmission requireOneByUpdatedAt(string $updated_at) Return the first ChildTowerRecSubmission filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTowerRecSubmission[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTowerRecSubmission objects based on current ModelCriteria
 * @method     ChildTowerRecSubmission[]|ObjectCollection findById(int $id) Return ChildTowerRecSubmission objects filtered by the id column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByName(string $name) Return ChildTowerRecSubmission objects filtered by the name column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByPartnerId(int $partner_id) Return ChildTowerRecSubmission objects filtered by the partner_id column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByLongitude(string $longitude) Return ChildTowerRecSubmission objects filtered by the longitude column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByLatitude(string $latitude) Return ChildTowerRecSubmission objects filtered by the latitude column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByCellPlanId(int $cell_plan_id) Return ChildTowerRecSubmission objects filtered by the cell_plan_id column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByDistanceFromCenter(double $distance_from_center) Return ChildTowerRecSubmission objects filtered by the distance_from_center column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByHeight(int $height) Return ChildTowerRecSubmission objects filtered by the height column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByDistrict(string $district) Return ChildTowerRecSubmission objects filtered by the district column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByVillage(string $village) Return ChildTowerRecSubmission objects filtered by the village column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByTowerType(string $tower_type) Return ChildTowerRecSubmission objects filtered by the tower_type column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByLocation(string $location) Return ChildTowerRecSubmission objects filtered by the location column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByReviewerNote(string $reviewer_note) Return ChildTowerRecSubmission objects filtered by the reviewer_note column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByStatus(string $status) Return ChildTowerRecSubmission objects filtered by the status column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildTowerRecSubmission objects filtered by the created_at column
 * @method     ChildTowerRecSubmission[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildTowerRecSubmission objects filtered by the updated_at column
 * @method     ChildTowerRecSubmission[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TowerRecSubmissionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TowerRecSubmissionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\TowerRecSubmission', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTowerRecSubmissionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTowerRecSubmissionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTowerRecSubmissionQuery) {
            return $criteria;
        }
        $query = new ChildTowerRecSubmissionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTowerRecSubmission|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TowerRecSubmissionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TowerRecSubmissionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTowerRecSubmission A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, partner_id, longitude, latitude, cell_plan_id, distance_from_center, height, district, village, tower_type, location, reviewer_note, status, created_at, updated_at FROM tower_rec_submission WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTowerRecSubmission $obj */
            $obj = new ChildTowerRecSubmission();
            $obj->hydrate($row);
            TowerRecSubmissionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTowerRecSubmission|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the partner_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPartnerId(1234); // WHERE partner_id = 1234
     * $query->filterByPartnerId(array(12, 34)); // WHERE partner_id IN (12, 34)
     * $query->filterByPartnerId(array('min' => 12)); // WHERE partner_id > 12
     * </code>
     *
     * @see       filterByPartner()
     *
     * @param     mixed $partnerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByPartnerId($partnerId = null, $comparison = null)
    {
        if (is_array($partnerId)) {
            $useMinMax = false;
            if (isset($partnerId['min'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_PARTNER_ID, $partnerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($partnerId['max'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_PARTNER_ID, $partnerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_PARTNER_ID, $partnerId, $comparison);
    }

    /**
     * Filter the query on the longitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLongitude('fooValue');   // WHERE longitude = 'fooValue'
     * $query->filterByLongitude('%fooValue%', Criteria::LIKE); // WHERE longitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $longitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByLongitude($longitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($longitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_LONGITUDE, $longitude, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude('fooValue');   // WHERE latitude = 'fooValue'
     * $query->filterByLatitude('%fooValue%', Criteria::LIKE); // WHERE latitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $latitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($latitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the cell_plan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCellPlanId(1234); // WHERE cell_plan_id = 1234
     * $query->filterByCellPlanId(array(12, 34)); // WHERE cell_plan_id IN (12, 34)
     * $query->filterByCellPlanId(array('min' => 12)); // WHERE cell_plan_id > 12
     * </code>
     *
     * @see       filterByCellPlan()
     *
     * @param     mixed $cellPlanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByCellPlanId($cellPlanId = null, $comparison = null)
    {
        if (is_array($cellPlanId)) {
            $useMinMax = false;
            if (isset($cellPlanId['min'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_CELL_PLAN_ID, $cellPlanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cellPlanId['max'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_CELL_PLAN_ID, $cellPlanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_CELL_PLAN_ID, $cellPlanId, $comparison);
    }

    /**
     * Filter the query on the distance_from_center column
     *
     * Example usage:
     * <code>
     * $query->filterByDistanceFromCenter(1234); // WHERE distance_from_center = 1234
     * $query->filterByDistanceFromCenter(array(12, 34)); // WHERE distance_from_center IN (12, 34)
     * $query->filterByDistanceFromCenter(array('min' => 12)); // WHERE distance_from_center > 12
     * </code>
     *
     * @param     mixed $distanceFromCenter The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByDistanceFromCenter($distanceFromCenter = null, $comparison = null)
    {
        if (is_array($distanceFromCenter)) {
            $useMinMax = false;
            if (isset($distanceFromCenter['min'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_DISTANCE_FROM_CENTER, $distanceFromCenter['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($distanceFromCenter['max'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_DISTANCE_FROM_CENTER, $distanceFromCenter['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_DISTANCE_FROM_CENTER, $distanceFromCenter, $comparison);
    }

    /**
     * Filter the query on the height column
     *
     * Example usage:
     * <code>
     * $query->filterByHeight(1234); // WHERE height = 1234
     * $query->filterByHeight(array(12, 34)); // WHERE height IN (12, 34)
     * $query->filterByHeight(array('min' => 12)); // WHERE height > 12
     * </code>
     *
     * @param     mixed $height The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByHeight($height = null, $comparison = null)
    {
        if (is_array($height)) {
            $useMinMax = false;
            if (isset($height['min'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_HEIGHT, $height['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($height['max'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_HEIGHT, $height['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_HEIGHT, $height, $comparison);
    }

    /**
     * Filter the query on the district column
     *
     * Example usage:
     * <code>
     * $query->filterByDistrict('fooValue');   // WHERE district = 'fooValue'
     * $query->filterByDistrict('%fooValue%', Criteria::LIKE); // WHERE district LIKE '%fooValue%'
     * </code>
     *
     * @param     string $district The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByDistrict($district = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($district)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_DISTRICT, $district, $comparison);
    }

    /**
     * Filter the query on the village column
     *
     * Example usage:
     * <code>
     * $query->filterByVillage('fooValue');   // WHERE village = 'fooValue'
     * $query->filterByVillage('%fooValue%', Criteria::LIKE); // WHERE village LIKE '%fooValue%'
     * </code>
     *
     * @param     string $village The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByVillage($village = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($village)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_VILLAGE, $village, $comparison);
    }

    /**
     * Filter the query on the tower_type column
     *
     * Example usage:
     * <code>
     * $query->filterByTowerType('fooValue');   // WHERE tower_type = 'fooValue'
     * $query->filterByTowerType('%fooValue%', Criteria::LIKE); // WHERE tower_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $towerType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByTowerType($towerType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($towerType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_TOWER_TYPE, $towerType, $comparison);
    }

    /**
     * Filter the query on the location column
     *
     * Example usage:
     * <code>
     * $query->filterByLocation('fooValue');   // WHERE location = 'fooValue'
     * $query->filterByLocation('%fooValue%', Criteria::LIKE); // WHERE location LIKE '%fooValue%'
     * </code>
     *
     * @param     string $location The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByLocation($location = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($location)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_LOCATION, $location, $comparison);
    }

    /**
     * Filter the query on the reviewer_note column
     *
     * Example usage:
     * <code>
     * $query->filterByReviewerNote('fooValue');   // WHERE reviewer_note = 'fooValue'
     * $query->filterByReviewerNote('%fooValue%', Criteria::LIKE); // WHERE reviewer_note LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reviewerNote The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByReviewerNote($reviewerNote = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reviewerNote)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_REVIEWER_NOTE, $reviewerNote, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(TowerRecSubmissionTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerRecSubmissionTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Partner object
     *
     * @param \Partner|ObjectCollection $partner The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByPartner($partner, $comparison = null)
    {
        if ($partner instanceof \Partner) {
            return $this
                ->addUsingAlias(TowerRecSubmissionTableMap::COL_PARTNER_ID, $partner->getId(), $comparison);
        } elseif ($partner instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TowerRecSubmissionTableMap::COL_PARTNER_ID, $partner->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPartner() only accepts arguments of type \Partner or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Partner relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function joinPartner($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Partner');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Partner');
        }

        return $this;
    }

    /**
     * Use the Partner relation Partner object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PartnerQuery A secondary query class using the current class as primary query
     */
    public function usePartnerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPartner($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Partner', '\PartnerQuery');
    }

    /**
     * Filter the query by a related \CellPlan object
     *
     * @param \CellPlan|ObjectCollection $cellPlan The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function filterByCellPlan($cellPlan, $comparison = null)
    {
        if ($cellPlan instanceof \CellPlan) {
            return $this
                ->addUsingAlias(TowerRecSubmissionTableMap::COL_CELL_PLAN_ID, $cellPlan->getId(), $comparison);
        } elseif ($cellPlan instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TowerRecSubmissionTableMap::COL_CELL_PLAN_ID, $cellPlan->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCellPlan() only accepts arguments of type \CellPlan or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CellPlan relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function joinCellPlan($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CellPlan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CellPlan');
        }

        return $this;
    }

    /**
     * Use the CellPlan relation CellPlan object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CellPlanQuery A secondary query class using the current class as primary query
     */
    public function useCellPlanQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCellPlan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CellPlan', '\CellPlanQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTowerRecSubmission $towerRecSubmission Object to remove from the list of results
     *
     * @return $this|ChildTowerRecSubmissionQuery The current query, for fluid interface
     */
    public function prune($towerRecSubmission = null)
    {
        if ($towerRecSubmission) {
            $this->addUsingAlias(TowerRecSubmissionTableMap::COL_ID, $towerRecSubmission->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tower_rec_submission table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TowerRecSubmissionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TowerRecSubmissionTableMap::clearInstancePool();
            TowerRecSubmissionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TowerRecSubmissionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TowerRecSubmissionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TowerRecSubmissionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TowerRecSubmissionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TowerRecSubmissionQuery
