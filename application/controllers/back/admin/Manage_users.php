<?php

class Manage_users extends MY_Controller{


  function __construct(){
    parent::__construct();
		$this->set_objname('User');
		$this->tpl = 'admin/users';

  }

  function get_json(){
    $this->objobj = UserQuery::create()->filterByRole('admin');
    parent::get_json();
  }



	function write($id=null){
		$data = parent::write($id);
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('back/admin/manage_users/detail/'.$data->getId());
		}
	}

  function change_password(){
    $userid = $this->input->post('UserId');
    $user = UserQuery::create()->findPk($userid);
    if($user){
      $user->setPassword($this->input->post('Password'));
      $user->save();
      $this->loging->add_entry('User',$user->getId(),'activity_modify');

  		redirect('back/admin/manage_users/detail/'.$user->getId());
    }
  }

  function delete($id){
		$data = parent::delete($id);
		redirect('back/admin/manage_users');
  }

}
