<?php

namespace Base;

use \CellPlan as ChildCellPlan;
use \CellPlanQuery as ChildCellPlanQuery;
use \Exception;
use \PDO;
use Map\CellPlanTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'cell_plan' table.
 *
 *
 *
 * @method     ChildCellPlanQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCellPlanQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildCellPlanQuery orderByLongitude($order = Criteria::ASC) Order by the longitude column
 * @method     ChildCellPlanQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method     ChildCellPlanQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildCellPlanQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCellPlanQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCellPlanQuery groupById() Group by the id column
 * @method     ChildCellPlanQuery groupByName() Group by the name column
 * @method     ChildCellPlanQuery groupByLongitude() Group by the longitude column
 * @method     ChildCellPlanQuery groupByLatitude() Group by the latitude column
 * @method     ChildCellPlanQuery groupByStatus() Group by the status column
 * @method     ChildCellPlanQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCellPlanQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCellPlanQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCellPlanQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCellPlanQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCellPlanQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCellPlanQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCellPlanQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCellPlanQuery leftJoinTowerRecSubmission($relationAlias = null) Adds a LEFT JOIN clause to the query using the TowerRecSubmission relation
 * @method     ChildCellPlanQuery rightJoinTowerRecSubmission($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TowerRecSubmission relation
 * @method     ChildCellPlanQuery innerJoinTowerRecSubmission($relationAlias = null) Adds a INNER JOIN clause to the query using the TowerRecSubmission relation
 *
 * @method     ChildCellPlanQuery joinWithTowerRecSubmission($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TowerRecSubmission relation
 *
 * @method     ChildCellPlanQuery leftJoinWithTowerRecSubmission() Adds a LEFT JOIN clause and with to the query using the TowerRecSubmission relation
 * @method     ChildCellPlanQuery rightJoinWithTowerRecSubmission() Adds a RIGHT JOIN clause and with to the query using the TowerRecSubmission relation
 * @method     ChildCellPlanQuery innerJoinWithTowerRecSubmission() Adds a INNER JOIN clause and with to the query using the TowerRecSubmission relation
 *
 * @method     ChildCellPlanQuery leftJoinTower($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tower relation
 * @method     ChildCellPlanQuery rightJoinTower($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tower relation
 * @method     ChildCellPlanQuery innerJoinTower($relationAlias = null) Adds a INNER JOIN clause to the query using the Tower relation
 *
 * @method     ChildCellPlanQuery joinWithTower($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tower relation
 *
 * @method     ChildCellPlanQuery leftJoinWithTower() Adds a LEFT JOIN clause and with to the query using the Tower relation
 * @method     ChildCellPlanQuery rightJoinWithTower() Adds a RIGHT JOIN clause and with to the query using the Tower relation
 * @method     ChildCellPlanQuery innerJoinWithTower() Adds a INNER JOIN clause and with to the query using the Tower relation
 *
 * @method     \TowerRecSubmissionQuery|\TowerQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCellPlan findOne(ConnectionInterface $con = null) Return the first ChildCellPlan matching the query
 * @method     ChildCellPlan findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCellPlan matching the query, or a new ChildCellPlan object populated from the query conditions when no match is found
 *
 * @method     ChildCellPlan findOneById(int $id) Return the first ChildCellPlan filtered by the id column
 * @method     ChildCellPlan findOneByName(string $name) Return the first ChildCellPlan filtered by the name column
 * @method     ChildCellPlan findOneByLongitude(string $longitude) Return the first ChildCellPlan filtered by the longitude column
 * @method     ChildCellPlan findOneByLatitude(string $latitude) Return the first ChildCellPlan filtered by the latitude column
 * @method     ChildCellPlan findOneByStatus(string $status) Return the first ChildCellPlan filtered by the status column
 * @method     ChildCellPlan findOneByCreatedAt(string $created_at) Return the first ChildCellPlan filtered by the created_at column
 * @method     ChildCellPlan findOneByUpdatedAt(string $updated_at) Return the first ChildCellPlan filtered by the updated_at column *

 * @method     ChildCellPlan requirePk($key, ConnectionInterface $con = null) Return the ChildCellPlan by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCellPlan requireOne(ConnectionInterface $con = null) Return the first ChildCellPlan matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCellPlan requireOneById(int $id) Return the first ChildCellPlan filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCellPlan requireOneByName(string $name) Return the first ChildCellPlan filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCellPlan requireOneByLongitude(string $longitude) Return the first ChildCellPlan filtered by the longitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCellPlan requireOneByLatitude(string $latitude) Return the first ChildCellPlan filtered by the latitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCellPlan requireOneByStatus(string $status) Return the first ChildCellPlan filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCellPlan requireOneByCreatedAt(string $created_at) Return the first ChildCellPlan filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCellPlan requireOneByUpdatedAt(string $updated_at) Return the first ChildCellPlan filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCellPlan[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCellPlan objects based on current ModelCriteria
 * @method     ChildCellPlan[]|ObjectCollection findById(int $id) Return ChildCellPlan objects filtered by the id column
 * @method     ChildCellPlan[]|ObjectCollection findByName(string $name) Return ChildCellPlan objects filtered by the name column
 * @method     ChildCellPlan[]|ObjectCollection findByLongitude(string $longitude) Return ChildCellPlan objects filtered by the longitude column
 * @method     ChildCellPlan[]|ObjectCollection findByLatitude(string $latitude) Return ChildCellPlan objects filtered by the latitude column
 * @method     ChildCellPlan[]|ObjectCollection findByStatus(string $status) Return ChildCellPlan objects filtered by the status column
 * @method     ChildCellPlan[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCellPlan objects filtered by the created_at column
 * @method     ChildCellPlan[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCellPlan objects filtered by the updated_at column
 * @method     ChildCellPlan[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CellPlanQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CellPlanQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CellPlan', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCellPlanQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCellPlanQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCellPlanQuery) {
            return $criteria;
        }
        $query = new ChildCellPlanQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCellPlan|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CellPlanTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CellPlanTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCellPlan A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, longitude, latitude, status, created_at, updated_at FROM cell_plan WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCellPlan $obj */
            $obj = new ChildCellPlan();
            $obj->hydrate($row);
            CellPlanTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCellPlan|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CellPlanTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CellPlanTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CellPlanTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CellPlanTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CellPlanTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CellPlanTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the longitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLongitude('fooValue');   // WHERE longitude = 'fooValue'
     * $query->filterByLongitude('%fooValue%', Criteria::LIKE); // WHERE longitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $longitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByLongitude($longitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($longitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CellPlanTableMap::COL_LONGITUDE, $longitude, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude('fooValue');   // WHERE latitude = 'fooValue'
     * $query->filterByLatitude('%fooValue%', Criteria::LIKE); // WHERE latitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $latitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($latitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CellPlanTableMap::COL_LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CellPlanTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CellPlanTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CellPlanTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CellPlanTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CellPlanTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CellPlanTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CellPlanTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \TowerRecSubmission object
     *
     * @param \TowerRecSubmission|ObjectCollection $towerRecSubmission the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByTowerRecSubmission($towerRecSubmission, $comparison = null)
    {
        if ($towerRecSubmission instanceof \TowerRecSubmission) {
            return $this
                ->addUsingAlias(CellPlanTableMap::COL_ID, $towerRecSubmission->getCellPlanId(), $comparison);
        } elseif ($towerRecSubmission instanceof ObjectCollection) {
            return $this
                ->useTowerRecSubmissionQuery()
                ->filterByPrimaryKeys($towerRecSubmission->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTowerRecSubmission() only accepts arguments of type \TowerRecSubmission or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TowerRecSubmission relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function joinTowerRecSubmission($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TowerRecSubmission');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TowerRecSubmission');
        }

        return $this;
    }

    /**
     * Use the TowerRecSubmission relation TowerRecSubmission object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TowerRecSubmissionQuery A secondary query class using the current class as primary query
     */
    public function useTowerRecSubmissionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTowerRecSubmission($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TowerRecSubmission', '\TowerRecSubmissionQuery');
    }

    /**
     * Filter the query by a related \Tower object
     *
     * @param \Tower|ObjectCollection $tower the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCellPlanQuery The current query, for fluid interface
     */
    public function filterByTower($tower, $comparison = null)
    {
        if ($tower instanceof \Tower) {
            return $this
                ->addUsingAlias(CellPlanTableMap::COL_ID, $tower->getCellPlanId(), $comparison);
        } elseif ($tower instanceof ObjectCollection) {
            return $this
                ->useTowerQuery()
                ->filterByPrimaryKeys($tower->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTower() only accepts arguments of type \Tower or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tower relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function joinTower($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tower');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tower');
        }

        return $this;
    }

    /**
     * Use the Tower relation Tower object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TowerQuery A secondary query class using the current class as primary query
     */
    public function useTowerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTower($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tower', '\TowerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCellPlan $cellPlan Object to remove from the list of results
     *
     * @return $this|ChildCellPlanQuery The current query, for fluid interface
     */
    public function prune($cellPlan = null)
    {
        if ($cellPlan) {
            $this->addUsingAlias(CellPlanTableMap::COL_ID, $cellPlan->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the cell_plan table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CellPlanTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CellPlanTableMap::clearInstancePool();
            CellPlanTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CellPlanTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CellPlanTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CellPlanTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CellPlanTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CellPlanQuery
