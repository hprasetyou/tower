<?php

use Base\TowerRecSubmission as BaseTowerRecSubmission;

/**
 * Skeleton subclass for representing a row from the 'tower_rec_submission' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TowerRecSubmission extends BaseTowerRecSubmission
{
  function get_letter(){
    $conf = ConfigurationQuery::create();
    $content = $conf->findOneByName('recommendation_letter_body')->getDataValue();

    $variables = array(
      "_Company_" => $this->getPartner()->getCompany()->getName(),
      "_CompanyCity_"=>$this->getPartner()->getCompany()->getAddress(),
      "_LetterNumber_"=>$this->getName(),
      "_LetterDate_"=>$this->getUpdatedAt()?date_format($this->getUpdatedAt(),'d M Y'):'',
      "_TowerAddr_"=>$this->getLocation(),
      "_TowerLatLng_"=> "Latitude: {$this->getLatitude()} Longitude: {$this->getLongitude()}",
      "_TowerHeight_"=> $this->getHeight(),
      "_TowerType_"=> $this->getTowerType()
    );
    foreach ($variables as $key => $value) {
      # code...
      $content = str_replace($key,$value,$content);
    }
    return $content;
  }
}
