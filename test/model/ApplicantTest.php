<?php


class ApplicantTest extends \PHPUnit_Framework_TestCase
{
    public function testConfirmApplicant()
    {
        include('./application/libraries/Database_connection.php');
        $conn = new Database_connection();
        $conn->connect();

        //check number of applicant in db
        $applicant_count_f = ApplicantQuery::create()->count();
        //check number of applicant in db
        $user_count_f =  UserQuery::create()->count();
        //check number of applicant in db
        $partner_count_f = PartnerQuery::create()->count();

        $applicant_f = ApplicantQuery::create()->findOne();

        //confirm applicant
        $applicant_f->confirm();

        $applicant_count_n = ApplicantQuery::create()->count();
        //check number of applicant in db
        $user_count_n =  UserQuery::create()->count();
        //check number of applicant in db
        $partner_count_n = PartnerQuery::create()->count();


        $this->assertTrue($applicant_count_f>$applicant_count_n);
        $this->assertTrue($user_count_f<$user_count_n);
        $this->assertTrue($partner_count_f<$partner_count_n);
    }
}
