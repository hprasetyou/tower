# Rekomendasi Perijinan Pendirian Menara#

## Requirements ##

+ php5.6+ (recommended 7)
+ php-Imagick
+ composer
+ mysql

## How to install ##
+ clone this repo
+ copy application/config/config.php.sample => application/config/config.php (replace setting with your own)
+ run composer install
+ copy propel.php.dist => propel.php (change value with your own database connection) 
+ run ./propel diff
+ run ./propel migrate
+ run php index.php cli/Seeder seed
+ add * * * * * php /path/to/project/index.php cli/Task run to crontab


