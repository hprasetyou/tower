<?php

class Manage_surveys extends MY_Controller{


  function __construct(){

    $this->pass_method = ['get_json'];
	  parent::__construct();
		$this->set_objname('Survey');
		$this->tpl = 'admin/surveys';

  }

  function get_ref($ref){
    $node = ['TowerRecSubmission','Tower'];
    foreach ($node as $key => $value) {
      # code...
      $c = "{$value}Query";
      $ref = $c::create()->findOneByName($ref);
      if($ref){
        $clow = strtolower($value);
        redirect("back/admin/manage_{$clow}s/detail/{$ref->getId()}");
      }
    }
  }


	function write($id=null){
		$data = parent::write($id);
    //delete previous so data
    SurveyOfficerQuery::create()->findBySurveyId($data->getId())->delete();
    foreach ($this->input->post('SurveyOfficer') as $key => $value) {
      # code...
      write_log("value $value");
      if($value>0){
        $so = new SurveyOfficer;
        $so->setSurveyId($data->getId());
        $so->setPartnerId($value);
        $so->setRole($this->input->post("SurveyRole[$key]"));
        $so->save();
      }
    }
    foreach (json_decode($this->input->post('Images')) as $key => $value) {
      # code...
      $att = AttachmentQuery::create()
      ->findPk($value);
      $att->setObjectId($data->getId())->save();
    }
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('back/admin/manage_surveys/detail/'.$data->getId());
		}
	}

  public function remove_image(){
    $att = AttachmentQuery::create()
    ->findPk($this->input->get('id'));
    $att->delete();
    redirect('back/admin/manage_surveys/detail/'.$this->input->get('survey').'#edit');
  }

  // public function detail($id, $render = "html")
  // {
  //     $survey = SurveyQuery::create()->findPk($id);
  //     $images = AttachmentQuery::create()
  //     ->filterByModel('Survey')
  //     ->filterByObjectId($survey->getId())
  //     ->find();
  //     if ($this->input->is_ajax_request()) {
  //         echo($this->outputstd?json_encode($survey):$survey->toJSON());
  //     } else {
  //         $this->template->render("admin/admin/surveys/form",
  //        array(
  //          'Survey'=> $survey,
  //          'Images'=>$images
  //        ));
  //     }
  // }



  function delete($id){
		$data = parent::delete($id);
		redirect('manage_surveys');
  }

}
