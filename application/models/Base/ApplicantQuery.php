<?php

namespace Base;

use \Applicant as ChildApplicant;
use \ApplicantQuery as ChildApplicantQuery;
use \Exception;
use \PDO;
use Map\ApplicantTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'applicant' table.
 *
 *
 *
 * @method     ChildApplicantQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildApplicantQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildApplicantQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildApplicantQuery orderByIdNumber($order = Criteria::ASC) Order by the id_number column
 * @method     ChildApplicantQuery orderByGender($order = Criteria::ASC) Order by the gender column
 * @method     ChildApplicantQuery orderByPosition($order = Criteria::ASC) Order by the position column
 * @method     ChildApplicantQuery orderByCompany($order = Criteria::ASC) Order by the company column
 * @method     ChildApplicantQuery orderByAddress($order = Criteria::ASC) Order by the address column
 * @method     ChildApplicantQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildApplicantQuery orderByPasswordDraft($order = Criteria::ASC) Order by the password_draft column
 * @method     ChildApplicantQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildApplicantQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildApplicantQuery groupById() Group by the id column
 * @method     ChildApplicantQuery groupByName() Group by the name column
 * @method     ChildApplicantQuery groupByEmail() Group by the email column
 * @method     ChildApplicantQuery groupByIdNumber() Group by the id_number column
 * @method     ChildApplicantQuery groupByGender() Group by the gender column
 * @method     ChildApplicantQuery groupByPosition() Group by the position column
 * @method     ChildApplicantQuery groupByCompany() Group by the company column
 * @method     ChildApplicantQuery groupByAddress() Group by the address column
 * @method     ChildApplicantQuery groupByStatus() Group by the status column
 * @method     ChildApplicantQuery groupByPasswordDraft() Group by the password_draft column
 * @method     ChildApplicantQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildApplicantQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildApplicantQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApplicantQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApplicantQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApplicantQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApplicantQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApplicantQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApplicant findOne(ConnectionInterface $con = null) Return the first ChildApplicant matching the query
 * @method     ChildApplicant findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApplicant matching the query, or a new ChildApplicant object populated from the query conditions when no match is found
 *
 * @method     ChildApplicant findOneById(int $id) Return the first ChildApplicant filtered by the id column
 * @method     ChildApplicant findOneByName(string $name) Return the first ChildApplicant filtered by the name column
 * @method     ChildApplicant findOneByEmail(string $email) Return the first ChildApplicant filtered by the email column
 * @method     ChildApplicant findOneByIdNumber(string $id_number) Return the first ChildApplicant filtered by the id_number column
 * @method     ChildApplicant findOneByGender(string $gender) Return the first ChildApplicant filtered by the gender column
 * @method     ChildApplicant findOneByPosition(string $position) Return the first ChildApplicant filtered by the position column
 * @method     ChildApplicant findOneByCompany(string $company) Return the first ChildApplicant filtered by the company column
 * @method     ChildApplicant findOneByAddress(string $address) Return the first ChildApplicant filtered by the address column
 * @method     ChildApplicant findOneByStatus(string $status) Return the first ChildApplicant filtered by the status column
 * @method     ChildApplicant findOneByPasswordDraft(string $password_draft) Return the first ChildApplicant filtered by the password_draft column
 * @method     ChildApplicant findOneByCreatedAt(string $created_at) Return the first ChildApplicant filtered by the created_at column
 * @method     ChildApplicant findOneByUpdatedAt(string $updated_at) Return the first ChildApplicant filtered by the updated_at column *

 * @method     ChildApplicant requirePk($key, ConnectionInterface $con = null) Return the ChildApplicant by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOne(ConnectionInterface $con = null) Return the first ChildApplicant matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApplicant requireOneById(int $id) Return the first ChildApplicant filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByName(string $name) Return the first ChildApplicant filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByEmail(string $email) Return the first ChildApplicant filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByIdNumber(string $id_number) Return the first ChildApplicant filtered by the id_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByGender(string $gender) Return the first ChildApplicant filtered by the gender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByPosition(string $position) Return the first ChildApplicant filtered by the position column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByCompany(string $company) Return the first ChildApplicant filtered by the company column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByAddress(string $address) Return the first ChildApplicant filtered by the address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByStatus(string $status) Return the first ChildApplicant filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByPasswordDraft(string $password_draft) Return the first ChildApplicant filtered by the password_draft column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByCreatedAt(string $created_at) Return the first ChildApplicant filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicant requireOneByUpdatedAt(string $updated_at) Return the first ChildApplicant filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApplicant[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApplicant objects based on current ModelCriteria
 * @method     ChildApplicant[]|ObjectCollection findById(int $id) Return ChildApplicant objects filtered by the id column
 * @method     ChildApplicant[]|ObjectCollection findByName(string $name) Return ChildApplicant objects filtered by the name column
 * @method     ChildApplicant[]|ObjectCollection findByEmail(string $email) Return ChildApplicant objects filtered by the email column
 * @method     ChildApplicant[]|ObjectCollection findByIdNumber(string $id_number) Return ChildApplicant objects filtered by the id_number column
 * @method     ChildApplicant[]|ObjectCollection findByGender(string $gender) Return ChildApplicant objects filtered by the gender column
 * @method     ChildApplicant[]|ObjectCollection findByPosition(string $position) Return ChildApplicant objects filtered by the position column
 * @method     ChildApplicant[]|ObjectCollection findByCompany(string $company) Return ChildApplicant objects filtered by the company column
 * @method     ChildApplicant[]|ObjectCollection findByAddress(string $address) Return ChildApplicant objects filtered by the address column
 * @method     ChildApplicant[]|ObjectCollection findByStatus(string $status) Return ChildApplicant objects filtered by the status column
 * @method     ChildApplicant[]|ObjectCollection findByPasswordDraft(string $password_draft) Return ChildApplicant objects filtered by the password_draft column
 * @method     ChildApplicant[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildApplicant objects filtered by the created_at column
 * @method     ChildApplicant[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildApplicant objects filtered by the updated_at column
 * @method     ChildApplicant[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApplicantQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ApplicantQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Applicant', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApplicantQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApplicantQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApplicantQuery) {
            return $criteria;
        }
        $query = new ChildApplicantQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApplicant|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApplicantTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApplicantTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApplicant A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, email, id_number, gender, position, company, address, status, password_draft, created_at, updated_at FROM applicant WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApplicant $obj */
            $obj = new ChildApplicant();
            $obj->hydrate($row);
            ApplicantTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApplicant|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApplicantTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApplicantTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApplicantTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApplicantTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the id_number column
     *
     * Example usage:
     * <code>
     * $query->filterByIdNumber('fooValue');   // WHERE id_number = 'fooValue'
     * $query->filterByIdNumber('%fooValue%', Criteria::LIKE); // WHERE id_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByIdNumber($idNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_ID_NUMBER, $idNumber, $comparison);
    }

    /**
     * Filter the query on the gender column
     *
     * Example usage:
     * <code>
     * $query->filterByGender('fooValue');   // WHERE gender = 'fooValue'
     * $query->filterByGender('%fooValue%', Criteria::LIKE); // WHERE gender LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gender The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByGender($gender = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gender)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_GENDER, $gender, $comparison);
    }

    /**
     * Filter the query on the position column
     *
     * Example usage:
     * <code>
     * $query->filterByPosition('fooValue');   // WHERE position = 'fooValue'
     * $query->filterByPosition('%fooValue%', Criteria::LIKE); // WHERE position LIKE '%fooValue%'
     * </code>
     *
     * @param     string $position The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByPosition($position = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($position)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_POSITION, $position, $comparison);
    }

    /**
     * Filter the query on the company column
     *
     * Example usage:
     * <code>
     * $query->filterByCompany('fooValue');   // WHERE company = 'fooValue'
     * $query->filterByCompany('%fooValue%', Criteria::LIKE); // WHERE company LIKE '%fooValue%'
     * </code>
     *
     * @param     string $company The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByCompany($company = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($company)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_COMPANY, $company, $comparison);
    }

    /**
     * Filter the query on the address column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE address = 'fooValue'
     * $query->filterByAddress('%fooValue%', Criteria::LIKE); // WHERE address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the password_draft column
     *
     * Example usage:
     * <code>
     * $query->filterByPasswordDraft('fooValue');   // WHERE password_draft = 'fooValue'
     * $query->filterByPasswordDraft('%fooValue%', Criteria::LIKE); // WHERE password_draft LIKE '%fooValue%'
     * </code>
     *
     * @param     string $passwordDraft The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByPasswordDraft($passwordDraft = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($passwordDraft)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_PASSWORD_DRAFT, $passwordDraft, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApplicantTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApplicantTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApplicantTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApplicantTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicantTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApplicant $applicant Object to remove from the list of results
     *
     * @return $this|ChildApplicantQuery The current query, for fluid interface
     */
    public function prune($applicant = null)
    {
        if ($applicant) {
            $this->addUsingAlias(ApplicantTableMap::COL_ID, $applicant->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the applicant table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicantTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApplicantTableMap::clearInstancePool();
            ApplicantTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicantTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApplicantTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApplicantTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApplicantTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApplicantQuery
