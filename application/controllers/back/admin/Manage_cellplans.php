<?php

class Manage_cellplans extends MY_Controller{


  function __construct(){
    $this->pass_method = ['get_all'];
    parent::__construct();
		$this->set_objname('CellPlan');
		$this->tpl = 'admin/cellplans';
  }


	function write($id=null){
		$data = parent::write($id);
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('manage_cellplans/detail/'.$data->getId());
		}
	}

  function get_all(){
    $cellplans = CellPlanQuery::create()->find();
    $o['radius'] = (ConfigurationQuery::create()->read_conf('cell_plan_radius'))*1;
    $o['cellplan'] = [];
    foreach ($cellplans as $key => $value) {
      # code...
      $o['cellplan'][$key]['name'] = $value->getName();
      $o['cellplan'][$key]['LatLng'] = array(
        'lat'=> ($value->getLatitude() * 1),
        'lng' => ($value->getLongitude() * 1)
      );
    }
    if($this->input->get('with_tower')){
      $towers = TowerQuery::create()->find();
      foreach ($towers as $key => $value) {
        $o['tower'][$key]['name'] = $value->getName();
        $o['tower'][$key]['LatLng'] = array(
          'lat'=> ($value->getLatitude() * 1),
          'lng' => ($value->getLongitude() * 1)
        );
      }
    }
    echo json_encode($o);
  }

  function delete($id){
		$data = parent::delete($id);
		redirect('manage_cellplans');
  }

}
