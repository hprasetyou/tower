<?php

namespace Base;

use \Tower as ChildTower;
use \TowerQuery as ChildTowerQuery;
use \Exception;
use \PDO;
use Map\TowerTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tower' table.
 *
 *
 *
 * @method     ChildTowerQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildTowerQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildTowerQuery orderByLongitude($order = Criteria::ASC) Order by the longitude column
 * @method     ChildTowerQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method     ChildTowerQuery orderByHeight($order = Criteria::ASC) Order by the height column
 * @method     ChildTowerQuery orderByLocation($order = Criteria::ASC) Order by the location column
 * @method     ChildTowerQuery orderByPartnerId($order = Criteria::ASC) Order by the partner_id column
 * @method     ChildTowerQuery orderByBuildDate($order = Criteria::ASC) Order by the build_date column
 * @method     ChildTowerQuery orderByLandOwner($order = Criteria::ASC) Order by the land_owner column
 * @method     ChildTowerQuery orderByCellPlanId($order = Criteria::ASC) Order by the cell_plan_id column
 * @method     ChildTowerQuery orderByTowerType($order = Criteria::ASC) Order by the tower_type column
 * @method     ChildTowerQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildTowerQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildTowerQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildTowerQuery groupById() Group by the id column
 * @method     ChildTowerQuery groupByName() Group by the name column
 * @method     ChildTowerQuery groupByLongitude() Group by the longitude column
 * @method     ChildTowerQuery groupByLatitude() Group by the latitude column
 * @method     ChildTowerQuery groupByHeight() Group by the height column
 * @method     ChildTowerQuery groupByLocation() Group by the location column
 * @method     ChildTowerQuery groupByPartnerId() Group by the partner_id column
 * @method     ChildTowerQuery groupByBuildDate() Group by the build_date column
 * @method     ChildTowerQuery groupByLandOwner() Group by the land_owner column
 * @method     ChildTowerQuery groupByCellPlanId() Group by the cell_plan_id column
 * @method     ChildTowerQuery groupByTowerType() Group by the tower_type column
 * @method     ChildTowerQuery groupByStatus() Group by the status column
 * @method     ChildTowerQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildTowerQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildTowerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTowerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTowerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTowerQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTowerQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTowerQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTowerQuery leftJoinPartner($relationAlias = null) Adds a LEFT JOIN clause to the query using the Partner relation
 * @method     ChildTowerQuery rightJoinPartner($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Partner relation
 * @method     ChildTowerQuery innerJoinPartner($relationAlias = null) Adds a INNER JOIN clause to the query using the Partner relation
 *
 * @method     ChildTowerQuery joinWithPartner($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Partner relation
 *
 * @method     ChildTowerQuery leftJoinWithPartner() Adds a LEFT JOIN clause and with to the query using the Partner relation
 * @method     ChildTowerQuery rightJoinWithPartner() Adds a RIGHT JOIN clause and with to the query using the Partner relation
 * @method     ChildTowerQuery innerJoinWithPartner() Adds a INNER JOIN clause and with to the query using the Partner relation
 *
 * @method     ChildTowerQuery leftJoinCellPlan($relationAlias = null) Adds a LEFT JOIN clause to the query using the CellPlan relation
 * @method     ChildTowerQuery rightJoinCellPlan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CellPlan relation
 * @method     ChildTowerQuery innerJoinCellPlan($relationAlias = null) Adds a INNER JOIN clause to the query using the CellPlan relation
 *
 * @method     ChildTowerQuery joinWithCellPlan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CellPlan relation
 *
 * @method     ChildTowerQuery leftJoinWithCellPlan() Adds a LEFT JOIN clause and with to the query using the CellPlan relation
 * @method     ChildTowerQuery rightJoinWithCellPlan() Adds a RIGHT JOIN clause and with to the query using the CellPlan relation
 * @method     ChildTowerQuery innerJoinWithCellPlan() Adds a INNER JOIN clause and with to the query using the CellPlan relation
 *
 * @method     ChildTowerQuery leftJoinRetribution($relationAlias = null) Adds a LEFT JOIN clause to the query using the Retribution relation
 * @method     ChildTowerQuery rightJoinRetribution($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Retribution relation
 * @method     ChildTowerQuery innerJoinRetribution($relationAlias = null) Adds a INNER JOIN clause to the query using the Retribution relation
 *
 * @method     ChildTowerQuery joinWithRetribution($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Retribution relation
 *
 * @method     ChildTowerQuery leftJoinWithRetribution() Adds a LEFT JOIN clause and with to the query using the Retribution relation
 * @method     ChildTowerQuery rightJoinWithRetribution() Adds a RIGHT JOIN clause and with to the query using the Retribution relation
 * @method     ChildTowerQuery innerJoinWithRetribution() Adds a INNER JOIN clause and with to the query using the Retribution relation
 *
 * @method     \PartnerQuery|\CellPlanQuery|\RetributionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTower findOne(ConnectionInterface $con = null) Return the first ChildTower matching the query
 * @method     ChildTower findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTower matching the query, or a new ChildTower object populated from the query conditions when no match is found
 *
 * @method     ChildTower findOneById(int $id) Return the first ChildTower filtered by the id column
 * @method     ChildTower findOneByName(string $name) Return the first ChildTower filtered by the name column
 * @method     ChildTower findOneByLongitude(string $longitude) Return the first ChildTower filtered by the longitude column
 * @method     ChildTower findOneByLatitude(string $latitude) Return the first ChildTower filtered by the latitude column
 * @method     ChildTower findOneByHeight(int $height) Return the first ChildTower filtered by the height column
 * @method     ChildTower findOneByLocation(string $location) Return the first ChildTower filtered by the location column
 * @method     ChildTower findOneByPartnerId(int $partner_id) Return the first ChildTower filtered by the partner_id column
 * @method     ChildTower findOneByBuildDate(string $build_date) Return the first ChildTower filtered by the build_date column
 * @method     ChildTower findOneByLandOwner(string $land_owner) Return the first ChildTower filtered by the land_owner column
 * @method     ChildTower findOneByCellPlanId(int $cell_plan_id) Return the first ChildTower filtered by the cell_plan_id column
 * @method     ChildTower findOneByTowerType(string $tower_type) Return the first ChildTower filtered by the tower_type column
 * @method     ChildTower findOneByStatus(string $status) Return the first ChildTower filtered by the status column
 * @method     ChildTower findOneByCreatedAt(string $created_at) Return the first ChildTower filtered by the created_at column
 * @method     ChildTower findOneByUpdatedAt(string $updated_at) Return the first ChildTower filtered by the updated_at column *

 * @method     ChildTower requirePk($key, ConnectionInterface $con = null) Return the ChildTower by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOne(ConnectionInterface $con = null) Return the first ChildTower matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTower requireOneById(int $id) Return the first ChildTower filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByName(string $name) Return the first ChildTower filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByLongitude(string $longitude) Return the first ChildTower filtered by the longitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByLatitude(string $latitude) Return the first ChildTower filtered by the latitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByHeight(int $height) Return the first ChildTower filtered by the height column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByLocation(string $location) Return the first ChildTower filtered by the location column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByPartnerId(int $partner_id) Return the first ChildTower filtered by the partner_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByBuildDate(string $build_date) Return the first ChildTower filtered by the build_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByLandOwner(string $land_owner) Return the first ChildTower filtered by the land_owner column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByCellPlanId(int $cell_plan_id) Return the first ChildTower filtered by the cell_plan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByTowerType(string $tower_type) Return the first ChildTower filtered by the tower_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByStatus(string $status) Return the first ChildTower filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByCreatedAt(string $created_at) Return the first ChildTower filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTower requireOneByUpdatedAt(string $updated_at) Return the first ChildTower filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTower[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTower objects based on current ModelCriteria
 * @method     ChildTower[]|ObjectCollection findById(int $id) Return ChildTower objects filtered by the id column
 * @method     ChildTower[]|ObjectCollection findByName(string $name) Return ChildTower objects filtered by the name column
 * @method     ChildTower[]|ObjectCollection findByLongitude(string $longitude) Return ChildTower objects filtered by the longitude column
 * @method     ChildTower[]|ObjectCollection findByLatitude(string $latitude) Return ChildTower objects filtered by the latitude column
 * @method     ChildTower[]|ObjectCollection findByHeight(int $height) Return ChildTower objects filtered by the height column
 * @method     ChildTower[]|ObjectCollection findByLocation(string $location) Return ChildTower objects filtered by the location column
 * @method     ChildTower[]|ObjectCollection findByPartnerId(int $partner_id) Return ChildTower objects filtered by the partner_id column
 * @method     ChildTower[]|ObjectCollection findByBuildDate(string $build_date) Return ChildTower objects filtered by the build_date column
 * @method     ChildTower[]|ObjectCollection findByLandOwner(string $land_owner) Return ChildTower objects filtered by the land_owner column
 * @method     ChildTower[]|ObjectCollection findByCellPlanId(int $cell_plan_id) Return ChildTower objects filtered by the cell_plan_id column
 * @method     ChildTower[]|ObjectCollection findByTowerType(string $tower_type) Return ChildTower objects filtered by the tower_type column
 * @method     ChildTower[]|ObjectCollection findByStatus(string $status) Return ChildTower objects filtered by the status column
 * @method     ChildTower[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildTower objects filtered by the created_at column
 * @method     ChildTower[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildTower objects filtered by the updated_at column
 * @method     ChildTower[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TowerQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TowerQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Tower', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTowerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTowerQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTowerQuery) {
            return $criteria;
        }
        $query = new ChildTowerQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTower|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TowerTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TowerTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTower A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, longitude, latitude, height, location, partner_id, build_date, land_owner, cell_plan_id, tower_type, status, created_at, updated_at FROM tower WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTower $obj */
            $obj = new ChildTower();
            $obj->hydrate($row);
            TowerTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTower|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TowerTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TowerTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TowerTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TowerTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the longitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLongitude('fooValue');   // WHERE longitude = 'fooValue'
     * $query->filterByLongitude('%fooValue%', Criteria::LIKE); // WHERE longitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $longitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByLongitude($longitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($longitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_LONGITUDE, $longitude, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude('fooValue');   // WHERE latitude = 'fooValue'
     * $query->filterByLatitude('%fooValue%', Criteria::LIKE); // WHERE latitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $latitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($latitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the height column
     *
     * Example usage:
     * <code>
     * $query->filterByHeight(1234); // WHERE height = 1234
     * $query->filterByHeight(array(12, 34)); // WHERE height IN (12, 34)
     * $query->filterByHeight(array('min' => 12)); // WHERE height > 12
     * </code>
     *
     * @param     mixed $height The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByHeight($height = null, $comparison = null)
    {
        if (is_array($height)) {
            $useMinMax = false;
            if (isset($height['min'])) {
                $this->addUsingAlias(TowerTableMap::COL_HEIGHT, $height['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($height['max'])) {
                $this->addUsingAlias(TowerTableMap::COL_HEIGHT, $height['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_HEIGHT, $height, $comparison);
    }

    /**
     * Filter the query on the location column
     *
     * Example usage:
     * <code>
     * $query->filterByLocation('fooValue');   // WHERE location = 'fooValue'
     * $query->filterByLocation('%fooValue%', Criteria::LIKE); // WHERE location LIKE '%fooValue%'
     * </code>
     *
     * @param     string $location The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByLocation($location = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($location)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_LOCATION, $location, $comparison);
    }

    /**
     * Filter the query on the partner_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPartnerId(1234); // WHERE partner_id = 1234
     * $query->filterByPartnerId(array(12, 34)); // WHERE partner_id IN (12, 34)
     * $query->filterByPartnerId(array('min' => 12)); // WHERE partner_id > 12
     * </code>
     *
     * @see       filterByPartner()
     *
     * @param     mixed $partnerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByPartnerId($partnerId = null, $comparison = null)
    {
        if (is_array($partnerId)) {
            $useMinMax = false;
            if (isset($partnerId['min'])) {
                $this->addUsingAlias(TowerTableMap::COL_PARTNER_ID, $partnerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($partnerId['max'])) {
                $this->addUsingAlias(TowerTableMap::COL_PARTNER_ID, $partnerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_PARTNER_ID, $partnerId, $comparison);
    }

    /**
     * Filter the query on the build_date column
     *
     * Example usage:
     * <code>
     * $query->filterByBuildDate('2011-03-14'); // WHERE build_date = '2011-03-14'
     * $query->filterByBuildDate('now'); // WHERE build_date = '2011-03-14'
     * $query->filterByBuildDate(array('max' => 'yesterday')); // WHERE build_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $buildDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByBuildDate($buildDate = null, $comparison = null)
    {
        if (is_array($buildDate)) {
            $useMinMax = false;
            if (isset($buildDate['min'])) {
                $this->addUsingAlias(TowerTableMap::COL_BUILD_DATE, $buildDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($buildDate['max'])) {
                $this->addUsingAlias(TowerTableMap::COL_BUILD_DATE, $buildDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_BUILD_DATE, $buildDate, $comparison);
    }

    /**
     * Filter the query on the land_owner column
     *
     * Example usage:
     * <code>
     * $query->filterByLandOwner('fooValue');   // WHERE land_owner = 'fooValue'
     * $query->filterByLandOwner('%fooValue%', Criteria::LIKE); // WHERE land_owner LIKE '%fooValue%'
     * </code>
     *
     * @param     string $landOwner The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByLandOwner($landOwner = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($landOwner)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_LAND_OWNER, $landOwner, $comparison);
    }

    /**
     * Filter the query on the cell_plan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCellPlanId(1234); // WHERE cell_plan_id = 1234
     * $query->filterByCellPlanId(array(12, 34)); // WHERE cell_plan_id IN (12, 34)
     * $query->filterByCellPlanId(array('min' => 12)); // WHERE cell_plan_id > 12
     * </code>
     *
     * @see       filterByCellPlan()
     *
     * @param     mixed $cellPlanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByCellPlanId($cellPlanId = null, $comparison = null)
    {
        if (is_array($cellPlanId)) {
            $useMinMax = false;
            if (isset($cellPlanId['min'])) {
                $this->addUsingAlias(TowerTableMap::COL_CELL_PLAN_ID, $cellPlanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cellPlanId['max'])) {
                $this->addUsingAlias(TowerTableMap::COL_CELL_PLAN_ID, $cellPlanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_CELL_PLAN_ID, $cellPlanId, $comparison);
    }

    /**
     * Filter the query on the tower_type column
     *
     * Example usage:
     * <code>
     * $query->filterByTowerType('fooValue');   // WHERE tower_type = 'fooValue'
     * $query->filterByTowerType('%fooValue%', Criteria::LIKE); // WHERE tower_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $towerType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByTowerType($towerType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($towerType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_TOWER_TYPE, $towerType, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(TowerTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(TowerTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(TowerTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(TowerTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TowerTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Partner object
     *
     * @param \Partner|ObjectCollection $partner The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTowerQuery The current query, for fluid interface
     */
    public function filterByPartner($partner, $comparison = null)
    {
        if ($partner instanceof \Partner) {
            return $this
                ->addUsingAlias(TowerTableMap::COL_PARTNER_ID, $partner->getId(), $comparison);
        } elseif ($partner instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TowerTableMap::COL_PARTNER_ID, $partner->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPartner() only accepts arguments of type \Partner or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Partner relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function joinPartner($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Partner');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Partner');
        }

        return $this;
    }

    /**
     * Use the Partner relation Partner object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PartnerQuery A secondary query class using the current class as primary query
     */
    public function usePartnerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPartner($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Partner', '\PartnerQuery');
    }

    /**
     * Filter the query by a related \CellPlan object
     *
     * @param \CellPlan|ObjectCollection $cellPlan The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTowerQuery The current query, for fluid interface
     */
    public function filterByCellPlan($cellPlan, $comparison = null)
    {
        if ($cellPlan instanceof \CellPlan) {
            return $this
                ->addUsingAlias(TowerTableMap::COL_CELL_PLAN_ID, $cellPlan->getId(), $comparison);
        } elseif ($cellPlan instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TowerTableMap::COL_CELL_PLAN_ID, $cellPlan->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCellPlan() only accepts arguments of type \CellPlan or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CellPlan relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function joinCellPlan($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CellPlan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CellPlan');
        }

        return $this;
    }

    /**
     * Use the CellPlan relation CellPlan object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CellPlanQuery A secondary query class using the current class as primary query
     */
    public function useCellPlanQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCellPlan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CellPlan', '\CellPlanQuery');
    }

    /**
     * Filter the query by a related \Retribution object
     *
     * @param \Retribution|ObjectCollection $retribution the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTowerQuery The current query, for fluid interface
     */
    public function filterByRetribution($retribution, $comparison = null)
    {
        if ($retribution instanceof \Retribution) {
            return $this
                ->addUsingAlias(TowerTableMap::COL_ID, $retribution->getTowerId(), $comparison);
        } elseif ($retribution instanceof ObjectCollection) {
            return $this
                ->useRetributionQuery()
                ->filterByPrimaryKeys($retribution->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRetribution() only accepts arguments of type \Retribution or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Retribution relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function joinRetribution($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Retribution');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Retribution');
        }

        return $this;
    }

    /**
     * Use the Retribution relation Retribution object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RetributionQuery A secondary query class using the current class as primary query
     */
    public function useRetributionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRetribution($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Retribution', '\RetributionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTower $tower Object to remove from the list of results
     *
     * @return $this|ChildTowerQuery The current query, for fluid interface
     */
    public function prune($tower = null)
    {
        if ($tower) {
            $this->addUsingAlias(TowerTableMap::COL_ID, $tower->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tower table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TowerTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TowerTableMap::clearInstancePool();
            TowerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TowerTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TowerTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TowerTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TowerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TowerQuery
