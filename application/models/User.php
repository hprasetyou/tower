<?php

use Base\User as BaseUser;

/**
 * Skeleton subclass for representing a row from the 'user' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class User extends BaseUser
{
    public function setPassword($v)
    {
        $options = [
            'cost' => 12,
        ];
        $pass = password_hash($v, PASSWORD_BCRYPT, $options);
        return parent::setPassword($pass);
    }

    public function checkPassword($pass)
    {
        if (password_verify($pass,$this->getPassword())) {
            return $this;
        } else {
            return false;
        }
    }
}
