<?php

class Manage_towerrecsubmissions extends MY_Controller
{
    public function __construct()
    {
        $this->user_role = 'applicant';
        parent::__construct();
        $this->set_objname('TowerRecSubmission');
        $this->tpl = 'applicant/towerrecsubmissions';
        // $this->authorization->check_authorization('manage_towerrecsubmissions');
    }

    public function get_json()
    {
        $this->objobj = TowerRecSubmissionQuery::create()
        ->filterByPartner(
        PartnerQuery::create()
        ->findByActiveUser()
      );
        parent::get_json();
    }

    public function create()
    {
        $tpl = $this->tpl;
        $this->template->render("admin/$tpl/form");
    }

    public function get_letter($id)
    {
        $tpl = $this->tpl;
        $towerrec = TowerRecSubmissionQuery::create()->findPk($id);
        $content = $towerrec->get_letter();
        $this->template->render_pdf("admin/$tpl/pdf/template",array(
          'content'=>$content
        ));
    }

    public function write($id=null)
    {
      $user = UserQuery::create()->findPk($this->session->uid);
        $this->form['PartnerId'] = array(
        'value'=> $user->getPartnerId()
      );
        $data = parent::write($id);
    		redirect('back/applicant/manage_towerrecsubmissions/detail/'.$data->getId());
    }

    public function delete($id)
    {
        $data = parent::delete($id);
        redirect('manage_towerrecsubmissions');
    }
}
