<?php

class Manage_internals extends MY_Controller{


  function __construct(){
    $this->pass_method = ['get_json','get_data'];
    parent::__construct();
		$this->set_objname('Internal');
		$this->tpl = 'admin/internals';
  }

  function get_data(){
    $internal = InternalQuery::create();
    if($this->input->get('q')){
      $colls = $this->schema->extract_fields('Partner');
      $except = array('Id','CreatedAt','UpdatedAt');
      $cond = [];
      foreach ($colls as $coll) {
        # code...
        if(!in_array($coll['Name'],$except)){
          if(!is_object($coll['Name'])){
            $cond[] = $coll['Name'];
            $internal->condition($coll['Name'] ,"Partner.$coll[Name] LIKE ?", "%".$this->input->get('q')."%");
          }
        }

      }
      $internal->where($cond,'or');
    }
    $o = [];
    foreach ($internal as $key => $value) {
      # code...
      $o[] = array(
        'id'=>$value->getId(),
        'text'=>$value->getName()
      );

    }
    echo json_encode($o);
  }
  function delete($id){
		$data = parent::delete($id);
		redirect('back/admin/manage_internals');
  }

  function write($id=null){
		$data = parent::write($id);
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('back/admin/manage_internals/detail/'.$data->getId());
		}
	}

}
