import 'jquery'
import 'bootstrap';
import 'datatables.net';
import 'datatables.net-bs';
import Dropzone from 'dropzone';

Dropzone.autoDiscover = false;
// or disable for specific dropzone:
Dropzone.options.surveyImages = false;

$(function() {
$("div#survey_images").addClass('dropzone')
var dz = new Dropzone("div#survey_images", { url: "/index.php/back/admin/manage_attachments/write"});
var images = []
dz.on('success',function(file,response){
  response = JSON.parse(response)
  images.push(response.Id)
  console.log(images);
  $('#inputImages').val(JSON.stringify(images))
})
dz.on('sending', function(file, xhr, formData){
            formData.append('Model', 'Survey');
        });
  // Dropzone.options.surveyImages = {
  //   init: function() {
  //     this.on("addedfile", function(file) { alert("Added file."); });
  //   }
  // };
})
