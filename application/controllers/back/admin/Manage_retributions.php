<?php

class Manage_retributions extends MY_Controller{


  function __construct(){
    $this->pass_method = ['get_json'];
    parent::__construct();
		$this->set_objname('Retribution');
		$this->tpl = 'retributions';
  }


	function write($id=null){
		$data = parent::write($id);
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('manage_retributions/detail/'.$data->getId());
		}
	}

  function delete($id){
		$data = parent::delete($id);
		redirect('manage_retributions');
  }

}
