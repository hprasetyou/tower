<?php

class Manage_attachments extends MY_Controller
{
    public function __construct()
    {
        $this->pass_method = ['write'];

        $this->user_role = 'applicant';
        parent::__construct();
        $this->set_objname('Attachment');
        $this->tpl = 'admin/attachment';
    }
    public function write($id=null)
    {
      print($this->input->post('image'));
        $config['upload_path']='./public/upload/images';
        $config['allowed_types']        = 'gif|jpg|png';
        $this->load->library('upload', $config);
        if($this->upload->do_upload('file')){
        $data = $this->upload->data();

        $image = new Imagick(glob("./public/upload/images/{$data['file_name']}"));
        $image->resizeImage(200,200,Imagick::FILTER_LANCZOS,1);
        $image->writeImages("./public/upload/images/thumb/{$data['file_name']}", true);
        $attachment = new Attachment();
        $attachment->setModel($this->input->post('Model'));
        $attachment->setName($data['file_name']);
        $attachment->setPath($data['full_path']);
        $attachment->save();
        echo $attachment->toJSON();
      }else{
        echo json_encode($this->upload->display_errors());
      }
    }
}
