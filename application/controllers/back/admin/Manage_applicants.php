<?php

class Manage_applicants extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->set_objname('Applicant');
        $this->tpl = 'admin/applicants';

        // $this->authorization->check_authorization('manage_applicants');
    }

    public function confirm($id)
    {
        $applicant = ApplicantQuery::create()
        ->findPk($id);
        if ($applicant) {
            $req = TowerRecSubmissionQuery::create()->findPk($id);
            $msg = array(
            'recipient'=>$req->getPartner()->getEmail(),
            'subject'=>'Pemberitahuan',
            'recipient_name'=>$req->getPartner()->getName(),
            'mail_tmpl' => 'mail/accepted_registration',
            'mail_tmpl_data'=>array(
              'recipient_name' => $applicant->getName(),
              'id_number' => $applicant->getIdNumber()
                )
            );
            $this->load->helper('send_mail');
            queue_message($msg);
            $applicant->confirm();
            $this->session->set_flashdata('success','Permohonan dikonfirmasi');

        }
        redirect('back/admin/manage_applicants');
    }
}
