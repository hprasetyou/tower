<?php

use Propel\Runtime\ActiveQuery\Criteria;
class Manage_groups extends MY_Controller{


  function __construct(){
    parent::__construct();
		$this->set_objname('Group');
		$this->tpl = 'admin/groups';
  }

  function detail($id,$render = "html"){
    $menus = MenuQuery::create()
	    ->filterByUserRole('admin')
	    ->filterByParentId(0,'>')->find();
    foreach($menus as $menu){
	    $access = MenuGroupQuery::create()
		    ->filterByGroupId($id)
		    ->filterByMenuId($menu->getId())
		    ->findOne();
	    $menu->Access = $access;
    }
    $group = GroupQuery::create()
	    ->findPk($id);
    $this->template->render('admin/admin/groups/form',array('menus'=>$menus,'Group'=>$group));
  }


  function write($id=null){
	  $this->form['Access']='Access';
		$data = parent::write($id);
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('back/admin/manage_groups/detail/'.$data->getId());
		}
	}

  function delete($id){
		$data = parent::delete($id);
		redirect('manage_groups');
  }

}
