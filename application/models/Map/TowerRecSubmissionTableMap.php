<?php

namespace Map;

use \TowerRecSubmission;
use \TowerRecSubmissionQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'tower_rec_submission' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TowerRecSubmissionTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.TowerRecSubmissionTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'tower_rec_submission';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\TowerRecSubmission';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'TowerRecSubmission';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the id field
     */
    const COL_ID = 'tower_rec_submission.id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'tower_rec_submission.name';

    /**
     * the column name for the partner_id field
     */
    const COL_PARTNER_ID = 'tower_rec_submission.partner_id';

    /**
     * the column name for the longitude field
     */
    const COL_LONGITUDE = 'tower_rec_submission.longitude';

    /**
     * the column name for the latitude field
     */
    const COL_LATITUDE = 'tower_rec_submission.latitude';

    /**
     * the column name for the cell_plan_id field
     */
    const COL_CELL_PLAN_ID = 'tower_rec_submission.cell_plan_id';

    /**
     * the column name for the distance_from_center field
     */
    const COL_DISTANCE_FROM_CENTER = 'tower_rec_submission.distance_from_center';

    /**
     * the column name for the height field
     */
    const COL_HEIGHT = 'tower_rec_submission.height';

    /**
     * the column name for the district field
     */
    const COL_DISTRICT = 'tower_rec_submission.district';

    /**
     * the column name for the village field
     */
    const COL_VILLAGE = 'tower_rec_submission.village';

    /**
     * the column name for the tower_type field
     */
    const COL_TOWER_TYPE = 'tower_rec_submission.tower_type';

    /**
     * the column name for the location field
     */
    const COL_LOCATION = 'tower_rec_submission.location';

    /**
     * the column name for the reviewer_note field
     */
    const COL_REVIEWER_NOTE = 'tower_rec_submission.reviewer_note';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'tower_rec_submission.status';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'tower_rec_submission.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'tower_rec_submission.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'PartnerId', 'Longitude', 'Latitude', 'CellPlanId', 'DistanceFromCenter', 'Height', 'District', 'Village', 'TowerType', 'Location', 'ReviewerNote', 'Status', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'partnerId', 'longitude', 'latitude', 'cellPlanId', 'distanceFromCenter', 'height', 'district', 'village', 'towerType', 'location', 'reviewerNote', 'status', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(TowerRecSubmissionTableMap::COL_ID, TowerRecSubmissionTableMap::COL_NAME, TowerRecSubmissionTableMap::COL_PARTNER_ID, TowerRecSubmissionTableMap::COL_LONGITUDE, TowerRecSubmissionTableMap::COL_LATITUDE, TowerRecSubmissionTableMap::COL_CELL_PLAN_ID, TowerRecSubmissionTableMap::COL_DISTANCE_FROM_CENTER, TowerRecSubmissionTableMap::COL_HEIGHT, TowerRecSubmissionTableMap::COL_DISTRICT, TowerRecSubmissionTableMap::COL_VILLAGE, TowerRecSubmissionTableMap::COL_TOWER_TYPE, TowerRecSubmissionTableMap::COL_LOCATION, TowerRecSubmissionTableMap::COL_REVIEWER_NOTE, TowerRecSubmissionTableMap::COL_STATUS, TowerRecSubmissionTableMap::COL_CREATED_AT, TowerRecSubmissionTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'partner_id', 'longitude', 'latitude', 'cell_plan_id', 'distance_from_center', 'height', 'district', 'village', 'tower_type', 'location', 'reviewer_note', 'status', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'PartnerId' => 2, 'Longitude' => 3, 'Latitude' => 4, 'CellPlanId' => 5, 'DistanceFromCenter' => 6, 'Height' => 7, 'District' => 8, 'Village' => 9, 'TowerType' => 10, 'Location' => 11, 'ReviewerNote' => 12, 'Status' => 13, 'CreatedAt' => 14, 'UpdatedAt' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'partnerId' => 2, 'longitude' => 3, 'latitude' => 4, 'cellPlanId' => 5, 'distanceFromCenter' => 6, 'height' => 7, 'district' => 8, 'village' => 9, 'towerType' => 10, 'location' => 11, 'reviewerNote' => 12, 'status' => 13, 'createdAt' => 14, 'updatedAt' => 15, ),
        self::TYPE_COLNAME       => array(TowerRecSubmissionTableMap::COL_ID => 0, TowerRecSubmissionTableMap::COL_NAME => 1, TowerRecSubmissionTableMap::COL_PARTNER_ID => 2, TowerRecSubmissionTableMap::COL_LONGITUDE => 3, TowerRecSubmissionTableMap::COL_LATITUDE => 4, TowerRecSubmissionTableMap::COL_CELL_PLAN_ID => 5, TowerRecSubmissionTableMap::COL_DISTANCE_FROM_CENTER => 6, TowerRecSubmissionTableMap::COL_HEIGHT => 7, TowerRecSubmissionTableMap::COL_DISTRICT => 8, TowerRecSubmissionTableMap::COL_VILLAGE => 9, TowerRecSubmissionTableMap::COL_TOWER_TYPE => 10, TowerRecSubmissionTableMap::COL_LOCATION => 11, TowerRecSubmissionTableMap::COL_REVIEWER_NOTE => 12, TowerRecSubmissionTableMap::COL_STATUS => 13, TowerRecSubmissionTableMap::COL_CREATED_AT => 14, TowerRecSubmissionTableMap::COL_UPDATED_AT => 15, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'partner_id' => 2, 'longitude' => 3, 'latitude' => 4, 'cell_plan_id' => 5, 'distance_from_center' => 6, 'height' => 7, 'district' => 8, 'village' => 9, 'tower_type' => 10, 'location' => 11, 'reviewer_note' => 12, 'status' => 13, 'created_at' => 14, 'updated_at' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tower_rec_submission');
        $this->setPhpName('TowerRecSubmission');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\TowerRecSubmission');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addForeignKey('partner_id', 'PartnerId', 'INTEGER', 'partner', 'id', false, null, null);
        $this->addColumn('longitude', 'Longitude', 'VARCHAR', true, 255, null);
        $this->addColumn('latitude', 'Latitude', 'VARCHAR', true, 255, null);
        $this->addForeignKey('cell_plan_id', 'CellPlanId', 'INTEGER', 'cell_plan', 'id', false, null, null);
        $this->addColumn('distance_from_center', 'DistanceFromCenter', 'FLOAT', false, null, null);
        $this->addColumn('height', 'Height', 'INTEGER', true, null, null);
        $this->addColumn('district', 'District', 'VARCHAR', false, 255, null);
        $this->addColumn('village', 'Village', 'VARCHAR', false, 255, null);
        $this->addColumn('tower_type', 'TowerType', 'VARCHAR', false, 255, null);
        $this->addColumn('location', 'Location', 'VARCHAR', false, 255, null);
        $this->addColumn('reviewer_note', 'ReviewerNote', 'LONGVARCHAR', false, null, null);
        $this->addColumn('status', 'Status', 'CHAR', false, null, 'pending');
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Partner', '\\Partner', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':partner_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('CellPlan', '\\CellPlan', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':cell_plan_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TowerRecSubmissionTableMap::CLASS_DEFAULT : TowerRecSubmissionTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (TowerRecSubmission object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TowerRecSubmissionTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TowerRecSubmissionTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TowerRecSubmissionTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TowerRecSubmissionTableMap::OM_CLASS;
            /** @var TowerRecSubmission $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TowerRecSubmissionTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TowerRecSubmissionTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TowerRecSubmissionTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var TowerRecSubmission $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TowerRecSubmissionTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_ID);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_NAME);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_PARTNER_ID);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_LONGITUDE);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_LATITUDE);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_CELL_PLAN_ID);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_DISTANCE_FROM_CENTER);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_HEIGHT);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_DISTRICT);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_VILLAGE);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_TOWER_TYPE);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_LOCATION);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_REVIEWER_NOTE);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_STATUS);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(TowerRecSubmissionTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.partner_id');
            $criteria->addSelectColumn($alias . '.longitude');
            $criteria->addSelectColumn($alias . '.latitude');
            $criteria->addSelectColumn($alias . '.cell_plan_id');
            $criteria->addSelectColumn($alias . '.distance_from_center');
            $criteria->addSelectColumn($alias . '.height');
            $criteria->addSelectColumn($alias . '.district');
            $criteria->addSelectColumn($alias . '.village');
            $criteria->addSelectColumn($alias . '.tower_type');
            $criteria->addSelectColumn($alias . '.location');
            $criteria->addSelectColumn($alias . '.reviewer_note');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TowerRecSubmissionTableMap::DATABASE_NAME)->getTable(TowerRecSubmissionTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TowerRecSubmissionTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TowerRecSubmissionTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TowerRecSubmissionTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a TowerRecSubmission or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or TowerRecSubmission object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TowerRecSubmissionTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \TowerRecSubmission) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TowerRecSubmissionTableMap::DATABASE_NAME);
            $criteria->add(TowerRecSubmissionTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = TowerRecSubmissionQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TowerRecSubmissionTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TowerRecSubmissionTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the tower_rec_submission table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TowerRecSubmissionQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a TowerRecSubmission or Criteria object.
     *
     * @param mixed               $criteria Criteria or TowerRecSubmission object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TowerRecSubmissionTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from TowerRecSubmission object
        }

        if ($criteria->containsKey(TowerRecSubmissionTableMap::COL_ID) && $criteria->keyContainsValue(TowerRecSubmissionTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.TowerRecSubmissionTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = TowerRecSubmissionQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TowerRecSubmissionTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TowerRecSubmissionTableMap::buildTableMap();
