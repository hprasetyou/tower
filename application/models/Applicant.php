<?php

use Base\Applicant as BaseApplicant;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'applicant' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Applicant extends BaseApplicant
{

    public function saveForm($data)
    {
        foreach ($data as $key => $value) {
            # code...
            $method = "set$key";
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
        $this->save();
        return $this;
    }
    public function confirm(){
      $this->setStatus('accepted');
      $this->save();
      $company = new Partner;
      $partner = new Partner;
      $user = new User;
      $company->setName($this->getCompany());
      $company->setAddress($this->getAddress());
      $company->save();
      //create partner
      $partner->setName($this->getName());
      $partner->setEmail($this->getEmail());
      $partner->setIdNumber($this->getIdNumber());
      $partner->setIdType('KTP');
      $partner->setCompanyId($company->getId());
      $partner->save();

      //create user
      $user->setName($this->getIdNumber());
      $user->setPassword($this->getPasswordDraft());
      $user->setPartnerId($partner->getId());
      $user->setRole('applicant');
      $user->save();

      // $this->delete();
    }
}
