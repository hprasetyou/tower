<?php

namespace Map;

use \Applicant;
use \ApplicantQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'applicant' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApplicantTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ApplicantTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'applicant';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Applicant';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Applicant';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'applicant.id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'applicant.name';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'applicant.email';

    /**
     * the column name for the id_number field
     */
    const COL_ID_NUMBER = 'applicant.id_number';

    /**
     * the column name for the gender field
     */
    const COL_GENDER = 'applicant.gender';

    /**
     * the column name for the position field
     */
    const COL_POSITION = 'applicant.position';

    /**
     * the column name for the company field
     */
    const COL_COMPANY = 'applicant.company';

    /**
     * the column name for the address field
     */
    const COL_ADDRESS = 'applicant.address';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'applicant.status';

    /**
     * the column name for the password_draft field
     */
    const COL_PASSWORD_DRAFT = 'applicant.password_draft';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'applicant.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'applicant.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Email', 'IdNumber', 'Gender', 'Position', 'Company', 'Address', 'Status', 'PasswordDraft', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'email', 'idNumber', 'gender', 'position', 'company', 'address', 'status', 'passwordDraft', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApplicantTableMap::COL_ID, ApplicantTableMap::COL_NAME, ApplicantTableMap::COL_EMAIL, ApplicantTableMap::COL_ID_NUMBER, ApplicantTableMap::COL_GENDER, ApplicantTableMap::COL_POSITION, ApplicantTableMap::COL_COMPANY, ApplicantTableMap::COL_ADDRESS, ApplicantTableMap::COL_STATUS, ApplicantTableMap::COL_PASSWORD_DRAFT, ApplicantTableMap::COL_CREATED_AT, ApplicantTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'email', 'id_number', 'gender', 'position', 'company', 'address', 'status', 'password_draft', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Email' => 2, 'IdNumber' => 3, 'Gender' => 4, 'Position' => 5, 'Company' => 6, 'Address' => 7, 'Status' => 8, 'PasswordDraft' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'email' => 2, 'idNumber' => 3, 'gender' => 4, 'position' => 5, 'company' => 6, 'address' => 7, 'status' => 8, 'passwordDraft' => 9, 'createdAt' => 10, 'updatedAt' => 11, ),
        self::TYPE_COLNAME       => array(ApplicantTableMap::COL_ID => 0, ApplicantTableMap::COL_NAME => 1, ApplicantTableMap::COL_EMAIL => 2, ApplicantTableMap::COL_ID_NUMBER => 3, ApplicantTableMap::COL_GENDER => 4, ApplicantTableMap::COL_POSITION => 5, ApplicantTableMap::COL_COMPANY => 6, ApplicantTableMap::COL_ADDRESS => 7, ApplicantTableMap::COL_STATUS => 8, ApplicantTableMap::COL_PASSWORD_DRAFT => 9, ApplicantTableMap::COL_CREATED_AT => 10, ApplicantTableMap::COL_UPDATED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'email' => 2, 'id_number' => 3, 'gender' => 4, 'position' => 5, 'company' => 6, 'address' => 7, 'status' => 8, 'password_draft' => 9, 'created_at' => 10, 'updated_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('applicant');
        $this->setPhpName('Applicant');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Applicant');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('id_number', 'IdNumber', 'VARCHAR', true, 255, null);
        $this->addColumn('gender', 'Gender', 'CHAR', false, null, null);
        $this->addColumn('position', 'Position', 'VARCHAR', false, 255, null);
        $this->addColumn('company', 'Company', 'VARCHAR', false, 255, null);
        $this->addColumn('address', 'Address', 'VARCHAR', false, 255, null);
        $this->addColumn('status', 'Status', 'CHAR', false, null, 'pending');
        $this->addColumn('password_draft', 'PasswordDraft', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApplicantTableMap::CLASS_DEFAULT : ApplicantTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Applicant object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApplicantTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApplicantTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApplicantTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApplicantTableMap::OM_CLASS;
            /** @var Applicant $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApplicantTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApplicantTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApplicantTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Applicant $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApplicantTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApplicantTableMap::COL_ID);
            $criteria->addSelectColumn(ApplicantTableMap::COL_NAME);
            $criteria->addSelectColumn(ApplicantTableMap::COL_EMAIL);
            $criteria->addSelectColumn(ApplicantTableMap::COL_ID_NUMBER);
            $criteria->addSelectColumn(ApplicantTableMap::COL_GENDER);
            $criteria->addSelectColumn(ApplicantTableMap::COL_POSITION);
            $criteria->addSelectColumn(ApplicantTableMap::COL_COMPANY);
            $criteria->addSelectColumn(ApplicantTableMap::COL_ADDRESS);
            $criteria->addSelectColumn(ApplicantTableMap::COL_STATUS);
            $criteria->addSelectColumn(ApplicantTableMap::COL_PASSWORD_DRAFT);
            $criteria->addSelectColumn(ApplicantTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ApplicantTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.id_number');
            $criteria->addSelectColumn($alias . '.gender');
            $criteria->addSelectColumn($alias . '.position');
            $criteria->addSelectColumn($alias . '.company');
            $criteria->addSelectColumn($alias . '.address');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.password_draft');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApplicantTableMap::DATABASE_NAME)->getTable(ApplicantTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApplicantTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApplicantTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApplicantTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Applicant or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Applicant object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicantTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Applicant) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApplicantTableMap::DATABASE_NAME);
            $criteria->add(ApplicantTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ApplicantQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApplicantTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApplicantTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the applicant table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApplicantQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Applicant or Criteria object.
     *
     * @param mixed               $criteria Criteria or Applicant object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicantTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Applicant object
        }

        if ($criteria->containsKey(ApplicantTableMap::COL_ID) && $criteria->keyContainsValue(ApplicantTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ApplicantTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ApplicantQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApplicantTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApplicantTableMap::buildTableMap();
