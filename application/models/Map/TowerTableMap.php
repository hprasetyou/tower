<?php

namespace Map;

use \Tower;
use \TowerQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'tower' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TowerTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.TowerTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'tower';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Tower';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Tower';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'tower.id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'tower.name';

    /**
     * the column name for the longitude field
     */
    const COL_LONGITUDE = 'tower.longitude';

    /**
     * the column name for the latitude field
     */
    const COL_LATITUDE = 'tower.latitude';

    /**
     * the column name for the height field
     */
    const COL_HEIGHT = 'tower.height';

    /**
     * the column name for the location field
     */
    const COL_LOCATION = 'tower.location';

    /**
     * the column name for the partner_id field
     */
    const COL_PARTNER_ID = 'tower.partner_id';

    /**
     * the column name for the build_date field
     */
    const COL_BUILD_DATE = 'tower.build_date';

    /**
     * the column name for the land_owner field
     */
    const COL_LAND_OWNER = 'tower.land_owner';

    /**
     * the column name for the cell_plan_id field
     */
    const COL_CELL_PLAN_ID = 'tower.cell_plan_id';

    /**
     * the column name for the tower_type field
     */
    const COL_TOWER_TYPE = 'tower.tower_type';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'tower.status';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'tower.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'tower.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Longitude', 'Latitude', 'Height', 'Location', 'PartnerId', 'BuildDate', 'LandOwner', 'CellPlanId', 'TowerType', 'Status', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'longitude', 'latitude', 'height', 'location', 'partnerId', 'buildDate', 'landOwner', 'cellPlanId', 'towerType', 'status', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(TowerTableMap::COL_ID, TowerTableMap::COL_NAME, TowerTableMap::COL_LONGITUDE, TowerTableMap::COL_LATITUDE, TowerTableMap::COL_HEIGHT, TowerTableMap::COL_LOCATION, TowerTableMap::COL_PARTNER_ID, TowerTableMap::COL_BUILD_DATE, TowerTableMap::COL_LAND_OWNER, TowerTableMap::COL_CELL_PLAN_ID, TowerTableMap::COL_TOWER_TYPE, TowerTableMap::COL_STATUS, TowerTableMap::COL_CREATED_AT, TowerTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'longitude', 'latitude', 'height', 'location', 'partner_id', 'build_date', 'land_owner', 'cell_plan_id', 'tower_type', 'status', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Longitude' => 2, 'Latitude' => 3, 'Height' => 4, 'Location' => 5, 'PartnerId' => 6, 'BuildDate' => 7, 'LandOwner' => 8, 'CellPlanId' => 9, 'TowerType' => 10, 'Status' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'longitude' => 2, 'latitude' => 3, 'height' => 4, 'location' => 5, 'partnerId' => 6, 'buildDate' => 7, 'landOwner' => 8, 'cellPlanId' => 9, 'towerType' => 10, 'status' => 11, 'createdAt' => 12, 'updatedAt' => 13, ),
        self::TYPE_COLNAME       => array(TowerTableMap::COL_ID => 0, TowerTableMap::COL_NAME => 1, TowerTableMap::COL_LONGITUDE => 2, TowerTableMap::COL_LATITUDE => 3, TowerTableMap::COL_HEIGHT => 4, TowerTableMap::COL_LOCATION => 5, TowerTableMap::COL_PARTNER_ID => 6, TowerTableMap::COL_BUILD_DATE => 7, TowerTableMap::COL_LAND_OWNER => 8, TowerTableMap::COL_CELL_PLAN_ID => 9, TowerTableMap::COL_TOWER_TYPE => 10, TowerTableMap::COL_STATUS => 11, TowerTableMap::COL_CREATED_AT => 12, TowerTableMap::COL_UPDATED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'longitude' => 2, 'latitude' => 3, 'height' => 4, 'location' => 5, 'partner_id' => 6, 'build_date' => 7, 'land_owner' => 8, 'cell_plan_id' => 9, 'tower_type' => 10, 'status' => 11, 'created_at' => 12, 'updated_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tower');
        $this->setPhpName('Tower');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Tower');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('longitude', 'Longitude', 'VARCHAR', true, 255, null);
        $this->addColumn('latitude', 'Latitude', 'VARCHAR', true, 255, null);
        $this->addColumn('height', 'Height', 'INTEGER', true, null, null);
        $this->addColumn('location', 'Location', 'VARCHAR', false, 255, null);
        $this->addForeignKey('partner_id', 'PartnerId', 'INTEGER', 'partner', 'id', false, null, null);
        $this->addColumn('build_date', 'BuildDate', 'DATE', false, null, null);
        $this->addColumn('land_owner', 'LandOwner', 'VARCHAR', false, 255, null);
        $this->addForeignKey('cell_plan_id', 'CellPlanId', 'INTEGER', 'cell_plan', 'id', false, null, null);
        $this->addColumn('tower_type', 'TowerType', 'VARCHAR', false, 255, null);
        $this->addColumn('status', 'Status', 'CHAR', true, null, 'new');
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Partner', '\\Partner', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':partner_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('CellPlan', '\\CellPlan', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':cell_plan_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Retribution', '\\Retribution', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':tower_id',
    1 => ':id',
  ),
), null, null, 'Retributions', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TowerTableMap::CLASS_DEFAULT : TowerTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Tower object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TowerTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TowerTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TowerTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TowerTableMap::OM_CLASS;
            /** @var Tower $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TowerTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TowerTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TowerTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Tower $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TowerTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TowerTableMap::COL_ID);
            $criteria->addSelectColumn(TowerTableMap::COL_NAME);
            $criteria->addSelectColumn(TowerTableMap::COL_LONGITUDE);
            $criteria->addSelectColumn(TowerTableMap::COL_LATITUDE);
            $criteria->addSelectColumn(TowerTableMap::COL_HEIGHT);
            $criteria->addSelectColumn(TowerTableMap::COL_LOCATION);
            $criteria->addSelectColumn(TowerTableMap::COL_PARTNER_ID);
            $criteria->addSelectColumn(TowerTableMap::COL_BUILD_DATE);
            $criteria->addSelectColumn(TowerTableMap::COL_LAND_OWNER);
            $criteria->addSelectColumn(TowerTableMap::COL_CELL_PLAN_ID);
            $criteria->addSelectColumn(TowerTableMap::COL_TOWER_TYPE);
            $criteria->addSelectColumn(TowerTableMap::COL_STATUS);
            $criteria->addSelectColumn(TowerTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(TowerTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.longitude');
            $criteria->addSelectColumn($alias . '.latitude');
            $criteria->addSelectColumn($alias . '.height');
            $criteria->addSelectColumn($alias . '.location');
            $criteria->addSelectColumn($alias . '.partner_id');
            $criteria->addSelectColumn($alias . '.build_date');
            $criteria->addSelectColumn($alias . '.land_owner');
            $criteria->addSelectColumn($alias . '.cell_plan_id');
            $criteria->addSelectColumn($alias . '.tower_type');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TowerTableMap::DATABASE_NAME)->getTable(TowerTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TowerTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TowerTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TowerTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Tower or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Tower object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TowerTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Tower) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TowerTableMap::DATABASE_NAME);
            $criteria->add(TowerTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = TowerQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TowerTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TowerTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the tower table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TowerQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Tower or Criteria object.
     *
     * @param mixed               $criteria Criteria or Tower object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TowerTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Tower object
        }

        if ($criteria->containsKey(TowerTableMap::COL_ID) && $criteria->keyContainsValue(TowerTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.TowerTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = TowerQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TowerTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TowerTableMap::buildTableMap();
